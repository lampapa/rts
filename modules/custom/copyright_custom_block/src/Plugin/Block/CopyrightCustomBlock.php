<?php

/**
 * @file
 * Contains \Drupal\copyright_custom_block\Plugin\Block
 */

namespace Drupal\copyright_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "copyright_custom_block",
 *  admin_label = @Translation("Copyright custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class CopyrightCustomBlock extends BlockBase{
 
    public function build(){
	   	$connection  = \Drupal::database();
	    $query = $connection->query("SELECT copy from catapult_footer");
	    while ($row = $query->fetchAssoc()){ 	      
	      $footerbar[] = $row;	      
	    }
	   /* echo "<pre>";
	    print_r($footerbar);
	    echo "</pre>";*/
	     return [
	      '#theme' => 'copyrighttemplate',
	      '#test_var' => $this->t('Test Value'),
	      '#items'=>$footerbar,
	    ];
  	}
}

?>