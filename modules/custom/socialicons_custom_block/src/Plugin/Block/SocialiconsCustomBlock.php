<?php

/**
 * @file
 * Contains \Drupal\socialicons_custom_block\Plugin\Block
 */

namespace Drupal\socialicons_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "socialicons_custom_block",
 *  admin_label = @Translation("Socialicons custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class SocialiconsCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'socialiconstemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>