<?php

/**
 * @file
 * Contains \Drupal\mainservices_custom_block\Plugin\Block
 */

namespace Drupal\mainservices_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "mainservices_custom_block",
 *  admin_label = @Translation("Main Services Custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class MainServicesBlock extends BlockBase{
  public function build(){

    $home_array = '';
   /* $nids = \Drupal::entityQuery('node')->condition('type','hometransmission')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $home_array = $node->body->value;
    } */
    return array(
    '#theme' => 'mainservicestemplate',
    '#test_var' => $this->t('Test Value'),
    '#items'=>''
    );
  }
   
}

?>