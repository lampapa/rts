<?php

/**
 * @file
 * Contains \Drupal\locations_footer_block\Plugin\Block
 */

namespace Drupal\locations_footer_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "locations_footer_block",
 *  admin_label = @Translation("Locations Footer Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class LocationsFooterBlock extends BlockBase{
 
  /* public function build(){
   	$connection = \Drupal::database();
	   	$loca = [];
	    $query = $connection->query("SELECT * FROM catapult_our_locations");
	    while($row = $query->fetchAssoc()){
	     $loca[] = array(
					        'heading'			=>$row['heading'],
					        'pagelink'			=>$row['pagelink']
					     );
	    }
      return [
      '#theme' => 'locationstemplate',
      '#loca' =>$loca,
    ];
  }*/

  public function build() {

    $service_array = [];
    $nids = \Drupal::entityQuery('node')->condition('type','location_footer')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);  
      $service_array[] = array($node->getTitle(),$ids);
    } 



 



   /* $nids = \Drupal::entityQuery('node')->condition('type','location_footer')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);  
      
      
      $service_array[] = array($node->body->value);
    } */
    /*print_r($service_array);
    exit;*/
    return array(
    '#theme' => 'locationstemplate',
    '#loca'=>$service_array
    );
  }
}

?>