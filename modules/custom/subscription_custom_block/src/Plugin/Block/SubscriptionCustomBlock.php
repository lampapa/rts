<?php

/**
 * @file
 * Contains \Drupal\subscription_custom_block\Plugin\Block
 */

namespace Drupal\subscription_custom_block\Plugin\Block;

require 'cronfile/sendgrid-php/vendor/autoload.php';

use SendGrid\Mail\Mail;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "subscription_custom_block",
 *  admin_label = @Translation("Subscription custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */

 
class SubscriptionCustomBlock extends BlockBase {
 
   public function build() {
   	$success_status = "";
   	if(isset($_SESSION['postid']) ){
			if($_SESSION['postid'] == ""){
				$_SESSION['postid'] = rand(10,100);
			}
		}else{
			$_SESSION['postid'] = rand(10,100);
		}
   	if(!empty($_POST)){
      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
	        if($_SESSION['postid'] == $_POST['postid']){

				$db = \Drupal::database();
				$result = $db->query("SELECT * from tbl_smtp_setting where status='Y'");
				while ($row = $result->fetchAssoc()) {
					$smtp_settings[] = $row;
				}

				$date = date("Y-m-d H:i:s");
	   		$emailid = $_POST['email'];

	   		$result = $db->insert('newsletter_subscribe')->fields([
                  'email_id' => $emailid,
                  'created_on' => $date
                ])->execute();

	            try {
	            	$subject ='RTS  | '.date('d-m-Y');
		            $message ='<html>
		                            <head>
		                            <title>RTS -Allison</title>

		                            </head>
		                            <body>
		                                <div class="" style="
		                            width: 100%;    max-width: 850px;
		                            margin: 0 auto;background: #fff
		                            ">
		                                    <div class="" style="background: #fff;
		                                    width: 100%;    max-width: 800px;margin: 0 auto">
		                                            <!-- <div class="" style="background: #65bb7e;
		                                    position: relative;
		                                    height: 150px;
		                                    margin: 0 auto;"></div> -->
		                                            
		                                            <div class="table-outer" style="     border-radius: 5px;
		                            background: #fff;
		                            width: 100%;
		                            max-width: 600px;
		                            margin: 0 auto;
		                            padding: 25px 15px 10px 15px;
		                            box-shadow: 0px 0px 10px #e4e4e4;">
		                                                
		                                        <p style="text-align: center;"><img src="https://www.apacatapult.com/rtsallison/sites/default/files/logo_light.png" ></p>

		                                                <p style="font-size: 14px;">Hi, </p>

		                                               
		                                                <p style="font-size: 14px;padding-left: 15px; ">Thank you for your subscription..</p>
		                                                <p style="font-size: 14px;padding-left: 15px; ">Note : This is an automated computer generated report.Please don\'t reply to this mail.</p>

		                                                <p style="font-size: 14px;">Regards, </p>
		                                                <p style="font-size: 14px;">RTS-Allison</p>

		                                            </div>

		                                    </div>

		                                </div>
		                            </body>
		                        </html>';
		                        $to = $emailid;
	  
			        	 	$email = new Mail();
		          		$email->setFrom($smtp_settings[0]['emailid'], $smtp_settings[0]['username']);
		          		$email->setSubject($subject);
		          		$email->addTo($to, 'RTS-Allison');
		         //  $email->addBcc("balaji.kr@apaengineering.com", "balaji");
		          $email->addContent("text/html", $message);

		           $sendgrid = new \SendGrid($smtp_settings[0]['api_key']);
		            $response = $sendgrid->send($email);
			          if($response->statusCode() == 202){
			            $success_status = "Subscribed Successfully";
			          }else{
							echo false;

						 }
			 } catch (Exception $e) {
	                echo false;
	        	}
			}
	 	} 
	 	$_SESSION['postid'] = "";	 		
	}
		if($_SESSION['postid'] == ""){
			$_SESSION['postid'] = rand(10,100);      
		} 
      return [
      '#theme' => 'subscriptiontemplate',
      '#title' => $success_status,
      '#test_var' => $this->t('Test Value'),
       '#postid'=>$_SESSION['postid'],
    ];
  }

}

?>