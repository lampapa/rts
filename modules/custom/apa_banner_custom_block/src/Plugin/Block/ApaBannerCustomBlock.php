<?php

/**
 * @file
 * Contains \Drupal\my_custom_block\Plugin\Block
 */

namespace Drupal\apa_banner_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
/**
 * below section is important
 * 
 * @Block(
 *  id = "apabanner_custom_block",
 *  admin_label = @Translation("Apa Banner custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class ApaBannerCustomBlock extends BlockBase{
 
   public function build(){
   	$service_array =[];
  	$nids          = \Drupal::entityQuery('node')->condition('type','bottom_banner')->execute();
  	foreach($nids as $key => $ids){	  			  		
		$node = \Drupal\node\Entity\Node::load($ids);						
		$service_array[] = array(							       
					        'file'=>file_create_url($node->field_bottom_banner_upload->entity->getFileUri()),
					        'id'=>$ids,
					        'altvalue'=>$node->field_bottom_banner_upload->alt
    						);
		
	}

      return [
      '#theme' => 'apabannertemplate',
      '#test_var' => $this->t('Test Value'),
      '#service_array'=>$service_array
    ];
  }
}

?>