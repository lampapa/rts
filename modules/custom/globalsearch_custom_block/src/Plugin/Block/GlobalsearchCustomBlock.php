<?php

/**
 * @file
 * Contains \Drupal\globalsearch_custom_block\Plugin\Block
 */

namespace Drupal\globalsearch_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "globalsearch_custom_block",
 *  admin_label = @Translation("Globalsearch custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class GlobalsearchCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'globalsearchtemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>