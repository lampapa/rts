<?php

/**
 * @file
 * Contains \Drupal\hometransmission_custom_block\Plugin\Block
 */

namespace Drupal\hometransmission_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "hometransmission_custom_block",
 *  admin_label = @Translation("Home Transmission Custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class HomeTransmissionBlock extends BlockBase{
  public function build(){

    $home_array = '';
    $nids = \Drupal::entityQuery('node')->condition('type','hometransmission')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $home_array = $node->body->value;
    } 
    return array(
    '#theme' => 'hometransmissiontemplate',
    '#test_var' => $this->t('Test Value'),
    '#items'=>$home_array
    );
  }
   
}

?>