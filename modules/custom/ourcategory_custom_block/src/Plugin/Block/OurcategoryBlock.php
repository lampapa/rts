<?php

/**
 * @file
 * Contains \Drupal\ourcategory_custom_block\Plugin\Block
 */

namespace Drupal\ourcategory_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "ourcategory_custom_block",
 *  admin_label = @Translation("Ourcategory Custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class OurcategoryBlock extends BlockBase{
  public function build(){
  	$connection = \Drupal::database();

     $query = $connection->query("SELECT NAME AS category_name,uri AS image_url,tid as category_id FROM taxonomy_term_field_data AS tfd LEFT JOIN taxonomy_term__field_category_image AS ci ON ci.entity_id = tfd.tid LEFT JOIN file_managed AS fm ON fm.fid =ci.field_category_image_target_id WHERE tfd.vid='category' ORDER BY tfd.tid DESC limit 0,4");
     while ($row = $query->fetchAssoc()) {
           $spliturl=str_replace('public://','',$row['image_url']);
       
        $product1 = array($spliturl,$row['category_name'],$row['category_id']);
        $category_array[] = $product1;
    }

      return [
      '#theme' => 'ourcategorytemplate',
      '#test_var' => $this->t('Test Value'),
      '#items'=>$category_array,
    ];
  }
   
}

?>