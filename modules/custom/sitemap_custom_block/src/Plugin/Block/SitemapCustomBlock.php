<?php

/**
 * @file
 * Contains \Drupal\sitemap_custom_block\Plugin\Block
 */

namespace Drupal\sitemap_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "sitemap_custom_block",
 *  admin_label = @Translation("Sitemap custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class SitemapCustomBlock extends BlockBase{
 
   public function build(){
      return [
      '#theme' => 'sitemaptemplate',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>