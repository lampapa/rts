<?php

/**
 * @file
 * Contains \Drupal\about_custom_block\Plugin\Block
 */

namespace Drupal\about_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "about_custom_block",
 *  admin_label = @Translation("About Custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class AboutBlock extends BlockBase{
  public function build(){
    /*  return [
      '#theme' => 'abouttemplate',
      '#test_var' => $this->t('Test Value'),
    ]; */
    $service_array =[];
    $nids = \Drupal::entityQuery('node')->condition('type','about_us')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $service_array[] = array(
      $node->getTitle(),$node->body->value,file_create_url($node->field_aboutusimage->entity->getFileUri())
      );
    } 
    return array(
    '#theme' => 'abouttemplate',
    '#test_var' => $this->t('Test Value'),
    '#items'=>$service_array
    //'#title'=>'Our Article List'
    );
  }
   
}

?>