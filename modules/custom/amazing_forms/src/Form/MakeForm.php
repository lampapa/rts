<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\amazing_forms\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Contribute form.
 */
class MakeForm extends FormBase {
  /**
   * {@inheritdoc}
   */
 public function getFormId() {
    return 'amazing_forms_make_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['make_id'] = array(
      '#type' => 'hidden',
      '#title' => t('make_id'),
      '#attributes' => array('class' => ['form-control'],'id'=>['make_id'])
    );
    $form['Make'] = array(
      '#type' => 'textfield',
      '#title' => t('Make'),
      '#attributes' => array('class' => ['form-control'],'id'=>['make'])
    );
   
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#attributes' => array('class' => ['button-normal btn-save-icon'])
    );
    $form['options']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array(''),
      '#attributes' => array('class' => ['button-normal btn-reset-icon'])
    );
    return $form;
  }
  public function MY_MODULE_FORM_ID_reset($form, &$form_state) {
    $form_state['rebuild'] = FALSE;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('Make') || empty($form_state->getValue('Make'))) {
        $form_state->setErrorByName('Make', $this->t('Please Enter Make'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $connection = \Drupal::database();
    $make =$makeid='';
    foreach ($form_state->getValues() as $key => $value) {
      //echo $key ."--textvalue--".$value;
      if($key == 'Make'){
        $make =$value;
      }
      if($key == 'make_id'){
        $makeid =$value;
      }
     
    }
    if($makeid ==''){
      $connection->query("insert into catapult_make_data(make,created_by,created_on,deletion_status) Values('".$make."','JP',Now(),'N')"); 
    }else{
      $connection->query("update catapult_make set make='".$make."',modified_by='JP',modified_on=Now() where make_id='".$makeid."'"); 
    }
       
  }
}