<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\amazing_forms\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Contribute form.
 */
class ModelForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazing_forms_model_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //  $connection = \Drupal::database();
    //   $query = $connection->query("SELECT make FROM catapult_make_data where deletion_status='N'");
    //   $arr = array();
    //   $arr[''] ='Select Make';
    //   while ($rec = $query->fetchAssoc()) {
    //    $arr[$rec['make']] = $rec['make'];
    
    //   } 
      
    
    // $form['Make'] = array(
    //   '#type' => 'select',
    //   '#title' => t('Make'),
    //   '#attributes' => array('class' => ['form-control']),
    //   '#options' => $arr
    // );

    /*echo "<pre>";
    print_r($form['Make']['#options']);
    die();*/
  
    $form['model_id'] = array(
      '#type' => 'hidden',
      '#title' => t('model_id'),
      '#attributes' => array('class' => ['form-control'],'id'=>['model_id'])
    );
     $form['Model'] = array(
      '#type' => 'textfield',
      '#title' => t('Model'),
      '#attributes' => array('class' => ['form-control'],'id'=>['model'])
    );
   
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#attributes' => array('class' => ['button-normal btn-save-icon'])
    );
    $form['options']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array(''),
      '#attributes' => array('class' => ['button-normal btn-reset-icon'])
    );
    return $form;
  }
  public function MY_MODULE_FORM_ID_reset($form, &$form_state) {
    $form_state['rebuild'] = FALSE;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
    if (!$form_state->getValue('Model') || empty($form_state->getValue('Model'))) {
        $form_state->setErrorByName('Model', $this->t('Please Enter Model'));
    }
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $connection = \Drupal::database();
    $model=$model_id='';
    foreach ($form_state->getValues() as $key => $value) {
      //echo $key ."--textvalue--".$value;
      
      if($key == 'Model'){
        $model =$value;
      }
      if($key == 'model_id'){
        $model_id =$value;
      }
    }
    if($model_id ==''){
     $connection->query("insert into catapult_model(model,created_by,created_on) Values('".$model."','JP',Now())");    
    }else{
      $connection->query("update catapult_model set model='".$model."',modified_by='JP',modified_on=Now() where model_id='".$model_id."'"); 
    }
  }
}