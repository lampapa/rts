<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\amazing_forms\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Contribute form.
 */
class AddApplicationForm extends FormBase {
  /**
   * {@inheritdoc}
   */
 public function getFormId() {
    return 'amazing_forms_addapplication_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $connection = \Drupal::database();
      $makequery = $connection->query("SELECT make FROM catapult_make_data where deletion_status='N'");
      $arrmake = array();
      $arrmake[''] ='Select Make';
      while ($rec = $makequery->fetchAssoc()) {
       $arrmake[$rec['make']] = $rec['make'];
      }
      $productquery = $connection->query("SELECT product_id,field_product_name_value as product_name FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id ORDER BY cp.product_id ASC");
      $arrproduct = array();
      $arrproduct[''] ='Select Product';
      while ($rec = $productquery->fetchAssoc()) {
        $arrproduct[$rec['product_id']] = $rec['product_name'];
       // $arrproduct[$rec['product_name']] = $rec['product_name'];
      }
    $form['Make'] = array(
      '#type' => 'select',
      '#title' => t('Make'),
      '#options' => $arrmake,
      '#attributes' => array('ng-change' => 'app_makeafteronchange()','class' => ['form-control'],'ng-model'=>['app_make_model'])
    );
    $form['Model'] = array(
      '#type' => 'select',
      '#title' => t('Model'),
      '#attributes' => array('ng-change' => 'app_getmodelonchange()','class' => ['form-control'],'ng-model'=>['app_model_model'],'id'=>['modelid'],'ng-options'=>['o for o in app_options track by o']),
       '#options' => array(
        '' => t('Select Model'),
      ),
    );
    $form['FromYear'] = array(
      '#type' => 'tel',
      '#title' => t('From Year'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['app_fromyear_model'])
    );
    $form['ToYear'] = array(
      '#type' => 'tel',
      '#title' => t('To Year'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['app_toyear_model'])
    );
     $form['Product'] = array(
      '#type' => 'select',
      '#title' => t('Product'),
      '#options' => $arrproduct,
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['app_product_model'])
    );

    $form['PartNumber'] = array(
      '#type' => 'textfield',
      '#title' => t('Part Number'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['app_partno_model'])
    );
     $form['Notes'] = array(
      '#type' => 'textarea',
      '#title' => t('Notes'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['app_notes_model'])
    );
    $form['savebutton'] = array(
      '#type' => 'button',
      '#value' => t('Save'),
      '#attributes' => array('ng-click' => 'saveapplicationdata()','id'=>['addapplication'],'class' => ['button-normal btn-save-icon' ]),
    );
    $form['options']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array(''),
       '#attributes' => array('class' => ['button-normal btn-reset-icon ']),
    );
    return $form;
  }
  public function MY_MODULE_FORM_ID_reset($form, &$form_state) {
    $form_state['rebuild'] = FALSE;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('Make') || empty($form_state->getValue('Make'))) {
        $form_state->setErrorByName('Make', $this->t('Please Enter Make'));
    }
    // Assert the lastname is valid
    if (!$form_state->getValue('Model') || empty($form_state->getValue('Model'))) {
        $form_state->setErrorByName('Model', $this->t('Please Enter Model'));
    }
    // Assert the email is valid
    // if (!$form_state->getValue('email') || !filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)) {
    //     $form_state->setErrorByName('email', $this->t('Votre adresse e-mail semble invalide.'));
    // }
    // Assert the subject is valid
    if (!$form_state->getValue('FromYear') || empty($form_state->getValue('FromYear'))) {
        $form_state->setErrorByName('FromYear', $this->t('Please Enter From Year'));
    }
    // Assert the message is valid
    if (!$form_state->getValue('ToYear') || empty($form_state->getValue('ToYear'))) {
        $form_state->setErrorByName('ToYear', $this->t('Plesae Enter To Year'));
    }
    if (!$form_state->getValue('PartNumber') || empty($form_state->getValue('PartNumber'))) {
        $form_state->setErrorByName('PartNumber', $this->t('Plesae Enter PartNo'));
    }
    if (!$form_state->getValue('Notes') || empty($form_state->getValue('Notes'))) {
        $form_state->setErrorByName('Notes', $this->t('Plesae Enter Notes'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {


     $connection = \Drupal::database();
    $make = $model=$from_year=$to_year=$partno=$notes ='';
    foreach ($form_state->getValues() as $key => $value) {
      //echo $key ."--textvalue--".$value;
      if($key == 'Make'){
        $make =$value;
      }
      if($key == 'Model'){
        $model =$value;
      }
      if($key == 'FromYear'){
        $from_year =$value;
      }
      if($key == 'ToYear'){
        $to_year =$value;
      }
      if($key == 'PartNumber'){
        $partno =$value;
      }
      if($key == 'Notes'){
        $notes =$value;
      }
    }
    if($from_year < $to_year){
      for($i=$from_year;$i<=$to_year;$i++){
        $query = $connection->query("insert into catapult_application_data(make,model,from_year,to_year,created_by,created_on,deletion_status,partno,notes,actual_year) Values('".$make."','".$model."','".$from_year."','".$to_year."','JP',Now(),'N','".$partno."','".$notes."','".$i."')");
      }
      drupal_set_message($this->t('Thank you very much for your message. You will receive a confirmation email shortly.'));
    }

  }
}
