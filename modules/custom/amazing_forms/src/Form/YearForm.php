<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\amazing_forms\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Contribute form.
 */
class YearForm extends FormBase {
  /**
   * {@inheritdoc}
   */
 public function getFormId() {
    return 'amazing_forms_year_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['year_id'] = array(
      '#type' => 'hidden',
      '#title' => t('year_id'),
      '#attributes' => array('class' => ['form-control'],'id'=>['year_id'])
    );
   
    $form['Year'] = array(
      '#type' => 'tel',
      '#title' => t('Year'),
      '#attributes' => array('class' => ['form-control'],'ng-model'=>['fromyear_model'],'id'=>['year'])
    );
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#attributes' => array('id'=>['myAnchor'],'class' => ['button-normal btn-save-icon']),
      '#value' => t('Save'),
    );
    
    $form['options']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array(''),
      '#attributes' => array('class' => ['button-normal btn-reset-icon'])
    );
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function MY_MODULE_FORM_ID_reset($form, &$form_state) {
    $form_state['rebuild'] = FALSE;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {
       
    // Assert the subject is valid
    if (!$form_state->getValue('Year') || empty($form_state->getValue('Year'))) {
        $form_state->setErrorByName('Year', $this->t('Please Enter From Year'));
    }
   
   
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {


     $connection = \Drupal::database();
    $year=$year_id='';
    foreach ($form_state->getValues() as $key => $value) {
      //echo $key ."--textvalue--".$value;
     
      if($key == 'Year'){
        $year =$value;
      } 
      if($key == 'year_id'){
        $year_id =$value;
      } 
    }
    if($year_id ==''){
      $connection->query("insert into catapult_year(year,created_by,created_on) Values('".$year."','JP',Now())");
    }else{
      $connection->query("update catapult_year set year='".$year."',modified_by='JP',modified_on=Now() where year_id='".$year_id."'");
    }
     
  }
}