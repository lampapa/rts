<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class Hino {

 public function page() {
  	
  	    return array(
        '#theme' => 'hino',
        '#items'=>'',
        '#seo_array' => ''
       
    );
  }

  public function technical_support() {
    
        return array(
        '#theme' => 'technical_support',
        '#items'=>'',
        '#seo_array' => ''
       
    );
  }
  
  public function allison_remans() {
    
        return array(
        '#theme' => 'allison_remans',
        '#items'=>'',
        '#seo_array' => ''
       
    );
  }
  

}