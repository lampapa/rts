<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */
namespace Drupal\drupalup_controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

include_once "modules/phpmailer/PHPMailer.php";
include_once "modules/phpmailer/SMTP.php";

class UserCreate{

  public function page() {

    $current= \Drupal::currentUser();
    $cur_id = $current->id();
    global $base_url;
    if ($cur_id != 0){    
        $response = new RedirectResponse($base_url);
        $response->send($base_url);  
    }  

    $connection     = \Drupal::database();   
    $success_status = "";
    $error          = "";
   if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }   
    if(!empty($_POST)){
        if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
            if($_SESSION['postid'] == $_POST['postid']){
                $ids = \Drupal::entityQuery('user')
                ->condition('name', $_POST['username'])
                ->range(0, 1)
                ->execute();
                if(!empty($ids)){
                    $error = "user already Exist";
                  //then this name already exists
                }else{
                    $password = $_POST['password'];
                    $username = $_POST['username'];
                    $email    = $_POST['email'];

                    
                    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
                    $user = \Drupal\user\Entity\User::create();
                    $user->setPassword($password);
                    $user->enforceIsNew();
                    $user->setEmail($email);
                    $user->setUsername($username);//This username must be unique and accept only a-Z,0-9, - _ @ .                
                    $user->activate();
                    $res = $user->save();
                    $success_status = "$base_url"; 

                    $to=$email;
                    $to_name=$username;
                    $subject="AUTOTECH USER CREDENTIAL";
                    $body=$this->mail_msg($username,$password,$email);
                    $mail_sent=$this->SendMail($to,$to_name,$subject,$body,$attachment='',$cc='',$bcc='');
                }                
            }   
        }
        $_SESSION['postid'] = "";
              
    }
    if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);      
    } 
    return array(
        '#theme' => 'user_create',
        '#postid'=>$_SESSION['postid'],
        '#error'=>$error,
        '#title'=>$success_status        
    );
  }


  function mail_msg($username,$password,$email){
    global $base_url;
    $html='';
    $html.='<div style="margin-left:auto;width:1000px;margin-right:auto">
           <table style=" font-family: Verdana; font-size: 14px; background-color: white; margin-bottom: 25px; width: 100%;">
           <tr>
           <td>
           <table width="100%" style="font-size:14px;border:solid 1px #ccc">
           <tbody>
           <tr>
           <td style="height:10px"></td>
           <td></td>
           </tr>
           <tr width="100%" style="width:100%">
           <td align="right" style="font-weight:bold!important;text-decoration:underline;font-size:18px;width:58%">
           <span style="vertical-align:sub">User Credentials </span>
           </td>
           <td style="width:42%;text-align:right">
           <span style="text-align:right;margin-right:10px">
            <img style="padding:5px;max-width:100%;width:200px;" src="http://61.16.143.98/autotech/sites/default/files/autotech_new.png" width="100">
           </span>
           </td>
           </tr>
           <tr>
           <td style="height:10px"></td>
           <td></td>
           </tr>
           </tbody>
           </table>
           </td>
           </tr>           
          <tr>
            <td>
              <table width="100%" cellspacing="0" style="font-size:14px;border:solid 1px #ccc">
                <tbody>
                  <tr>
                    <td width="50%" style="border-right:1px solid #ccc;padding-left:5px">
                    <table>
                      <tbody>
                        <tr>
                          <td><b>Username :</b> '.$username.'</td>
                        </tr>
                      </tbody>
                     </table>
                     </td>
                  </tr>
                </tbody>
              </table>
            </td>           
          </tr>
           <tr>
            <td>
              <table width="100%" cellspacing="0" style="font-size:14px;border:solid 1px #ccc">
                <tbody>
                  <tr>
                    <td width="50%" style="border-right:1px solid #ccc;padding-left:5px">
                    <table>
                      <tbody>
                        <tr>
                          <td><b>Password :</b> '.$password.'</td>
                        </tr>
                      </tbody>
                     </table>
                     </td>
                  </tr>
                </tbody>
              </table>
            </td>           
          </tr>
           <tr>
            <td>
              <table width="100%" cellspacing="0" style="font-size:14px;border:solid 1px #ccc">
                <tbody>
                  <tr>
                    <td width="50%" style="border-right:1px solid #ccc;padding-left:5px">
                    <table>
                      <tbody>
                        <tr>
                          <td><b>Email Address :</b> '.$email.'</td>
                        </tr>
                      </tbody>
                     </table>
                     </td>
                  </tr>
                </tbody>
              </table>
            </td>           
          </tr>
           </tbody>
          </table>
          </div>';
    return $html;

  }

  public function SendMail($to,$to_name='',$subject='',$body,$attachment='',$cc='',$bcc=''){

    $db = \Drupal::database();
    $result=$db->query("SELECT * from tbl_smtp_setting where status='Y'");
    while ($row = $result->fetchAssoc()) {
      $smtp_settings[]=$row;
     }
     if(!isset($smtp_settings)){
        return 'Error';
     }else{

      $mail = new \PHPMailer();
      $mail->isSMTP();
      $mail->SMTPDebug = 0;
      $mail->Host = $smtp_settings[0]['hostid'];
      $mail->Port = $smtp_settings[0]['portno'];
      $mail->SMTPSecure = $smtp_settings[0]['protocol'];
      $mail->SMTPAuth = true;
      $mail->Username = $smtp_settings[0]['emailid'];
      $mail->Password = $smtp_settings[0]['password'];
      $mail->setFrom($smtp_settings[0]['emailid'], $smtp_settings[0]['username']);
      //$mail->addReplyTo('reply-box@hostinger-tutorials.com', 'Your Name');
      $mail->addAddress($to, $to_name);
      $mail->addBCC($smtp_settings[0]['emailid']);
      //$mail->addBCC("autotech@apaengineering.com");
      $mail->Subject = $subject;
      $mail->msgHTML($body);
      // $mail->AltBody = 'This is a plain text message body';
      /*if($attachment){
        $mail->addAttachment($attachment);
      }

      if(!empty($cc)){
        if(is_array($cc)){
          foreach ($cc as $key => $value) {
            $mail->addCC($value);
          }
        }else{
          $mail->addCC($cc);
        }
      }

      if(!empty($bcc)){
        if(is_array($bcc)){
          foreach ($bcc as $key => $value) {
            $mail->addBCC($value);
          }
        }else{
          $mail->addBCC($bcc);
        }
      }*/
      // $mail->addAttachment('test.txt');
      if (!$mail->send()) {
        //die;        
        return 'Error';
      } else {
        return 'Sent';
      }
    }
    die();
  }

 
}