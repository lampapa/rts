<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class Careers {

  public function page() {
  	
  	    return array(
        '#theme' => 'rts_careers',
        '#items'=>'',
        '#seo_array' => ''
        //'#title'=>'Our Article List'
    );
  }

}