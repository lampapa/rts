<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class TermsandCondition {

  public function page() {

    $service_array = [];
    $nids = \Drupal::entityQuery('node')->condition('type','terms_and_conditions')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $service_array[] = array(
      $node->getTitle(),$node->body->value
      );
    } 
  //  echo "<pre>";
  //  print_r($service_array);
    return array(
    '#theme' => 'terms_and_conditions',
    '#items'=>$service_array
    //'#title'=>'Our Article List'
    );
  }

}

?>