<?php
	/**
	* @file
	* Contains \Drupal\hello\HelloController.
	*/

	namespace Drupal\drupalup_controller\Controller;
	use Drupal\Core\Controller\ControllerBase;
	use Symfony\Component\HttpFoundation\Request;
	use Drupal\Core\Database\Database;
	use Drupal\Core\Entity\Query\QueryFactory;

	include_once "modules/phpmailer/PHPMailer.php";
	include_once "modules/phpmailer/SMTP.php";

	class CovidTwo {

		public function page() {  	
			$error = '';$success = '';
			if(!isset($_SESSION['postid'])) {
				$_SESSION['postid'] = rand(10,100);      
			}

			if(!empty($_POST)){
				$fname=!empty($_POST['fname'])?$_POST['fname']:"";
				$lname=!empty($_POST['lname'])?$_POST['lname']:"";
				$mail_data=!empty($_POST['email'])?$_POST['email']:"";
				$phone=!empty($_POST['phone'])?$_POST['phone']:"";
				$order_num=!empty($_POST['order_num'])?$_POST['order_num']:"";                 
				$msg_data=!empty($_POST['message'])?$_POST['message']:"";
				$db = \Drupal::database();

				$name_data='RTAllison';

				$result = $db->insert('covid_contact_info')->fields([
				'fname' => $fname,
				'lname' => $lname,
				'phone' => $phone,
				'email' => $mail_data,                                   
				'message' => $msg_data,
				'order_num' => $order_num,
				])->execute();

				$subject_data='';
			    $mail_to='info@rtsallison.com'; 
				// $mail_to='md.jimmath@apaengineering.com';

				$msg_details='Name : '.$fname.' '.$lname.'<br> Mail Id : '.$mail_data.'<br>Contact Number : '
				.$phone.'<br> Order Number : '.$order_num.'<br> Message : '.$msg_data ;
				$mail_status=$this->SendMail($mail_to,$name_data,$subject_data,$msg_details);               
				if($mail_status=='Error'){
				$error='Mail Sending Failed';
				}elseif($mail_status=='Sent'){
				$success='Message sent!';
				}else{
				$error='Something Went Wrong';
				}                
				$_SESSION['postid'] = "";
			}
			if($_SESSION['postid'] == ""){
				$_SESSION['postid'] = rand(10,100);      
			}

			return array(
				'#theme' => 'covid_two_index',
				'#items'=>'',
				'#seo_array' => '',      
				'#success'=>$success,
				'#error'=>$error,
				'#postid'=>$_SESSION['postid']
			);
		}

		public function page_covidtwo() { 
			return array(
				'#theme' => 'covid_two_index_two',
				'#items'=>'',
				'#seo_array' => ''    
			);
		}

		public function page_diagnos() {      
			return array(
				'#theme' => 'covid_two_diagnos',
				'#items'=>'',
				'#seo_array' => ''
				//'#title'=>'Our Article List'
			);
		}
		public function page_gold() {      
			return array(
				'#theme' => 'covid_two_gold',
				'#items'=>'',
				'#seo_array' => ''
				//'#title'=>'Our Article List'
			);
		}
		public function page_specialist() {      
			return array(
				'#theme' => 'covid_two_specialist',
				'#items'=>'',
				'#seo_array' => ''
				//'#title'=>'Our Article List'
			);
		}
		public function page_quality() {      
			return array(
				'#theme' => 'covid_two_quality',
				'#items'=>'',
				'#seo_array' => ''
				//'#title'=>'Our Article List'
			);
		}

		public function SendMail($to,$to_name='',$subject='',$body,$attachment='',$cc='',$bcc=''){

			$db = \Drupal::database();
			$result=$db->query("SELECT * from tbl_smtp_setting where status='Y'");
			while ($row = $result->fetchAssoc()) {
				$smtp_settings[]=$row;
			}
			if(!isset($smtp_settings)){
				return 'Error';
			} else {
				$mail = new \PHPMailer();
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				$mail->Host = $smtp_settings[0]['hostid'];
				$mail->Port = $smtp_settings[0]['portno'];
				$mail->SMTPSecure = $smtp_settings[0]['protocol'];
				$mail->SMTPAuth = true;
				$mail->Username = $smtp_settings[0]['emailid'];
				$mail->Password = $smtp_settings[0]['password'];
				$mail->setFrom('tech@apaengineering.com', 'RTSAllision : Covid19');
				$mail->addAddress($to, 'RTSAllision');
				$mail->addBCC('saranya.m@apaengineering.com');
				$mail->addBCC('balaji.kr@apaengineering.com');
				$mail->addBCC('janakiraman.p@apaengineering.com');
				$mail->addBCC('nandhakumar.d@apaengineering.com');
				$mail->addBCC('mounica.a@apaengineering.com');
				$mail->Subject = 'Enquiry on Allison transmission maintenance program';
				$mail->msgHTML($body);
				if($attachment){
					$mail->addAttachment($attachment);
				}
				if (!$mail->send()) {
					return 'Error';
				} else {
					return 'Sent';
				}
			}
		}
	}
?>
