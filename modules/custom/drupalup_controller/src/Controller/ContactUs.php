<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\Query\QueryFactory;

include_once "modules/phpmailer/PHPMailer.php";
include_once "modules/phpmailer/SMTP.php";
// include_once "modules/phpmailer/Exception.php";

class ContactUs extends ControllerBase  {

  public function page(){
   
  // print_r($_POST);
   
  	$content = array('testing contact us');
  	if(!empty($_POST)){

                $name=!empty($_POST['cus_name'])?$_POST['cus_name']:"";
                $email=!empty($_POST['email'])?$_POST['email']:"";
                $company=!empty($_POST['company'])?$_POST['company']:"";
                $phone=!empty($_POST['phone'])?$_POST['phone']:"";
                $businesstype=!empty($_POST['businesstype'])?$_POST['businesstype']:"";                 
                $message=!empty($_POST['message'])?$_POST['message']:"";
                $db = \Drupal::database();

                $name_data='RTAllison';


                $result = $db->insert('contact_details')->fields([
                  'name' => $name,
                  'email' => $email,
                  'company' => $company,
                  'phone' => $phone,                                   
                  'businesstype' => $businesstype,
                  'message' => $message,
                ])->execute();

                $subject_data='Contact Details';
                //$mail_to='info@rtsallison.com';                
               //$mail_to='vaithi.d@apaengineering.com';
                if($businesstype == 'sales'){
                  $mail_to='marketing@rtsallison.com';
                }
                if($businesstype == 'partner'){
                  $mail_to='marketing@rtsallison.com';
                }
                //$mail_to='saranya.m@apaengineering.com';
                $msg_details='Name : '.$name.'<br> Mail Id : '.$email.'<br>Contact Number : '
                .$phone.'<br> Company : '.$company.'<br> Message : '.$message ;
                $mail_status=$this->SendMail($mail_to,$name_data,$subject_data,$msg_details);               
               // $mail_status=$this->SendMail($mail_data,$name_data,$subject_data,$msg_details);  
                if($mail_status=='Error'){
                  $error='Mail Sending Failed';
                }elseif($mail_status=='Sent'){
                  $success='Message sent!';
                }else{
                  $error='Something Went Wrong';
                }    
                          
    }
    
  
    return array(
        '#theme' => 'contact_us',
        '#items'=> ''
    //'#title'=>'Our Article List'
    );
  }

  public function SendMail($to,$to_name='',$subject='',$body,$attachment='',$cc='',$bcc=''){

    $db = \Drupal::database();
    $result=$db->query("SELECT * from tbl_smtp_setting where status='Y'");
    while ($row = $result->fetchAssoc()) {
      $smtp_settings[]=$row;
     }
     if(!isset($smtp_settings)){
        return 'Error';
     }else{
          
      $mail = new \PHPMailer();
      $mail->isSMTP();
      $mail->SMTPDebug = 0;
      $mail->Host = $smtp_settings[0]['hostid'];
      $mail->Port = $smtp_settings[0]['portno'];
      $mail->SMTPSecure = $smtp_settings[0]['protocol'];
      $mail->SMTPAuth = true;
      $mail->Username = $smtp_settings[0]['emailid'];
      $mail->Password = $smtp_settings[0]['password'];
     // $mail->setFrom('tech@apaengineerings.onmicrosoft.com', 'RTSAllision : Contact Us');
      $mail->setFrom($smtp_settings[0]['emailid'], $smtp_settings[0]['username']);
      //$mail->addReplyTo('reply-box@hostinger-tutorials.com', 'Your Name');
      $mail->addAddress($to, 'RTSAllision');
  //    $mail->addAddress('erich@rtsallison.com');
    //  $cc ='catapult@apaengineering.com';
     //$mail->addAddress('balaji.kr@apaengineering.com');

     
     // $cc ='balajikr6@gmail.com';
      //$mail->Subject = $subject;
      //$mail->addCC('vaithi.d@apaengineering.com');
     // $mail->addCC('nandini.k@apaengineering.com');


      
      $mail->Subject = 'RTS Allision - Contact Us';
      $mail->msgHTML($body);
      // $mail->AltBody = 'This is a plain text message body';
      if($attachment){
        $mail->addAttachment($attachment);
      }
      /*if(is_array($cc)){
        foreach ($cc as $key => $value) {
          $mail->addCC($value);
        }
      }else{
        $mail->addCC($cc);
      } 
      */
     /* if(is_array($bcc)){
        foreach ($bcc as $key => $value) {
          $mail->addBCC($value);
        }
      }else{
        $mail->addBCC($attachment);
      }*/
      // $mail->addAttachment('test.txt');
      if (!$mail->send()) {
        // $error='Mailer Error:' . $mail->ErrorInfo;
        return 'Error';
      } else {
        return 'Sent';
      }
    }
  }
}