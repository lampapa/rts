<?php

namespace Drupal\drupalup_controller\Controller;

class CartItems {

  public function cart_items() {
      
    $connection = \Drupal::database();
    $query = $connection->query("SELECT distinct purchased_entity as product_id FROM commerce_order_item");
    $arr = array();
    while($row = $query->fetchAssoc()){        
        //print_r($row);
        array_push($arr,$row['product_id']);
    }
    $product_ids = implode(',',$arr);   

    $query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id 
      LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id
      LEFT JOIN commerce_product__field_stock_info  AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id
      LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id LEFT JOIN file_managed AS fm ON fm.fid =pim.field_product_image_target_id where cp.product_id in(".$product_ids.") ORDER BY cp.product_id ASC");
  
      $var ='';    

      while($row = $query->fetchAssoc()){
        $spliturl=str_replace('public://','',$row['uri']);           
        $product1 = array($row['product_id'],$row['field_product_name_value'],$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number']),$spliturl,$row['field_wishlist_value']);
        $products[] = $product1;
        
        /*$p_id = $row['product_id'];
        $p_name =$row['field_product_name_value'];
        $p_desc =$row['field_new_feature_value'];
        $p_partno =$row['field_partno_value'];
        $p_stock =$row['field_stock_info_value'];
        $p_price =round($row['field_total_price_number']);
        $p_wishlist =$row['field_wishlist_value'];

      $var .="<li><div class='cart-list-left'><img src='sites/default/files/$spliturl' alt='' /></div><div class='lf-hle'><div class='lft'><div class='cart-list-right'><a class='word_break' href='#'>$p_name</a><span class='price_color'>$$p_price</span><span class='quantity'>Qty. : 1</span></div></div><div class='rgt'><i productid='$p_id' class='fa fa-close reomve_product'></i></div></div></li>";*/

      }
      //echo $products;   
      retrun $products;
  }
}