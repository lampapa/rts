<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class Home {

  public function page() {
    
  	$content = array('testing contact us');
    return array(
        '#theme' => 'home',
        '#items'=>$items
        //'#title'=>'Our Article List'
    );
  }
  public function getuserId(){

  	/** @var \Drupal\session_based_temp_store\SessionBasedTempStoreFactory $temp_store_factory */
	$temp_store_factory = \Drupal::service('session_based_temp_store');

	/** @var \Drupal\session_based_temp_store\SessionBasedTempStore $temp_store */
	$temp_store = $temp_store_factory->get('my_module_name', 4800); 
	// As the second argument you can optionally set the exparaion time. If not set, by default it equals 604800 which is 7 days.
	// As the third argument you can set the cookie path. The path on the server in which the cookie will be available on. By default, it's set to '/', it means the cookie will be available within the entire domain.

	
	

  	if(isset($_SESSION['_sf2_attributes']['uid']))
  	{
  		$return['userid'] 	= $_SESSION['_sf2_attributes']['uid'];
  		$return['url'] 		= $temp_store->get('last_url');
      $temp_store->delete('last_url');
  		echo json_encode($return);
  	}
  	else
  	{

  		$temp_store->set('last_url', $_POST['uris']);

  		$return['userid'] 	= 'not';
  		$return['url'] 		= $temp_store->get('last_url');
  		echo json_encode($return);
  	}
  	exit;
  }

}