<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Database\Database;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_price\Price;
use Drupal\taxonomy\Entity\Term;
include_once "modules/phpmailer/PHPMailer.php";
include_once "modules/phpmailer/SMTP.php";

class DrupalProductDesc {

  public function page($id){
  /*	echo $id;
  	die();*/
  	$items       = [];
  	$items_desc  = [];
  	$img_array   = [];
  	$body_content = [];
  	$item_link    = [];
  	$var_firt_array = [];
  	$var_second_array = [];
  	$vari_filter = '';
  	$interchange_array = [];
  	$term_name   = "";
	  	$user        = \Drupal::currentUser();
		$user_id     = $user->id();
	  	$connection  = \Drupal::database();

  	if( ( (isset($id)) && ($id != "") ) || ( (isset($_GET['ptno'])) && ($_GET['ptno'] != "") )  ){
		if( (isset($_GET['ptno'])) && ($_GET['ptno'] != "") ){
			$partno = $_GET['ptno'];
			//die();*/
			$query_desc = $connection->query("SELECT *,fmm.uri AS brand_uri,fm.uri as uri FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id 
				LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id
				LEFT JOIN commerce_product__field_stock_info  AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id
				LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id LEFT JOIN commerce_product__field_brand AS fb ON fb.entity_id =cp.product_id LEFT JOIN taxonomy_term__field_brand_image AS bi ON bi.entity_id =fb.field_brand_target_id LEFT JOIN file_managed AS fm ON fm.fid =pim.field_product_image_target_id LEFT JOIN file_managed AS fmm ON fmm.fid = bi.`field_brand_image_target_id`  WHERE pp.field_partno_value='".$partno."' ORDER BY cp.product_id");


		}else{

			$query_desc = $connection->query("SELECT *,fmm.uri AS brand_uri,fm.uri as uri FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id 
				LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id
				LEFT JOIN commerce_product__field_stock_info  AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id
				LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id LEFT JOIN commerce_product__field_brand AS fb ON fb.entity_id =cp.product_id LEFT JOIN taxonomy_term__field_brand_image AS bi ON bi.entity_id =fb.field_brand_target_id LEFT JOIN file_managed AS fm ON fm.fid =pim.field_product_image_target_id LEFT JOIN file_managed AS fmm ON fmm.fid = bi.`field_brand_image_target_id`  WHERE cp.product_id='".$id."' ORDER BY cp.product_id");
		}

	  	$record = $query_desc->fetchAssoc(); 
	  	$spliturl_desc=str_replace('public://','',$record['uri']);
	  	$brand_uri=str_replace('public://','',$record['brand_uri']);


	  	if(  (isset($_GET['ptno'])) && ($_GET['ptno'] != "")   ){
	  		/*echo "tes";
	  		echo "<pre>";
	  		print_r($record);
	  		die();*/
	  		$id = $record['product_id'];
	  		//die();
	  	}

	  	$discount_price =round($record['field_total_price_number'],1)+((10/100)*round($record['field_total_price_number'],1));
	  	$you_save=((10/100)*round($record['field_total_price_number'],1));

	    $product_detail           = \Drupal\commerce_product\Entity\Product::load($id);
	    $field_interchange='';

	    /*interchange part number ,sku, lester */

	   /* echo "<pre>";
	    print_r($product);
	    die();*/
	    $field_lester = '';
	    $field_interchange = '';
	  	if ( array_key_exists(0,$product_detail->get('field_lester')->getValue() ) ){
	    	$field_lester      =  $product_detail->get('field_lester')->getValue()[0]['value'];
	    }
	  	if ( array_key_exists(0,$product_detail->get('field_interchange')->getValue() ) ){
	  		$field_interchange  =  $product_detail->get('field_interchange')->getValue()[0]['value'];
	  	}
    	/*echo "<pre>";
    	print_r($product_detail->get('field_call_price')->getValue());
    	die();*/
    	if(!empty($product_detail->get('field_call_price')->getValue())){
    		if ( array_key_exists(0,$product_detail->get('field_call_price')->getValue() ) ){
    			
    			 $field_call_price               =  $product_detail->get('field_call_price')->getValue()[0]['value'];
    			//die();
	    		if($field_call_price  == null){
	    			$field_call_price = '';
	    		}   	
	    	}else{
	    		//die();
	    		$field_call_price               =  ''; 
	    		//die();
	    	
	    	}
    	}else{
    		$field_call_price               =  '';  
    		//die();
    	}
    	
    	

    	/*end of interchange part number ,sku, lester */


	    $entity_manager           = \Drupal::entityManager(); 
	    foreach($product_detail->getVariationIds() as $key=>$value){
	        $product_variation = $entity_manager->getStorage('commerce_product_variation')->load((int)$value); 
	        	

	       /*interchange part number ,sku, lester*/
	        $sku = '';
	       	if ( array_key_exists(0,$product_variation->get('sku')->getValue() ) ){
	    		$sku      =  $product_variation->get('sku')->getValue()[0]['value'];
	    	}

	       	//$sku = $product_variation->get('sku')->getValue()[0]['value'];	       
	        $interchange_array = array('sku'=>$sku,'field_interchange'=>$field_interchange,'field_lester'=>$field_lester,'field_call_price'=>$field_call_price);

	       /*end of interchange part number ,sku, lester*/



	        $currency_code = $product_variation->get('price')->getValue()[0]['currency_code'];

	        $prices = $product_variation->get('price')->getValue()[0]['number'];
	        

	        $data_color = $product_variation->get('field_color')->getValue()[0]['target_id'];	        	        
	        

	        $vid   = 'color_parent';
		    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid); 
		   	$prices = "";
		  	$currency_code="";
		  	$term_name="";
		  	$term_id="";
		  	$field_colorname="";
		    foreach ($terms as $term){
		    	$term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
		    	//if($term->tid != 152){
		    		if($data_color == $term->tid){
			    	  $field_colorname = $term_obj->get('field_colorname')->value;
			    	  $color_name = $term_obj->get('name')->value;
				      $term_id   = $term->tid;
				      $term_name = $term->name;
				      if($term->tid == 152){
				      	$var_firt_array[]=array($value,
		    					  $prices,
		    					  $currency_code,
		    					  $term_name,
		    					  $term_id,
		    					  $field_colorname,
		    					  $color_name);
				      }else{
				      		 $var_second_array[]=array($value,
		    					  $prices,
		    					  $currency_code,
		    					  $term_name,
		    					  $term_id,
		    					  $field_colorname,
		    					  $color_name);

				      }
				      
		  			}
		    	//}		    			     		     
		    }
		   
	      	      
     	} 
     	$vari_filter = array_merge($var_firt_array,$var_second_array);
     	/*echo "<pre>";
     	print_r($vari_filter);
     	die();*/
     	/*die();*/

	  	///// modified by nandha //// 
		$product1_desc = array($record['product_id'],$record['field_product_name_value'],$record['field_new_feature_value'],$record['field_partno_value'],$record['field_stock_info_value'],round($record['field_total_price_number'],2),$spliturl_desc,$record['field_wishlist_value'],$brand_uri,round($discount_price,2),$you_save,$vari_filter);
		$items_desc[] = $product1_desc;

		/*print_r($items_desc);
		exit;*/

		// Get image Attribute With id
		//$query_img=$connection->query("SELECT `field_image_one_target_id` FROM `commerce_product__field_image_one` WHERE entity_id='".$id."'");
		$img_array=array();
		if(isset($spliturl_desc)){

			$img_array[0]=$spliturl_desc;
		}

		$subquery = db_select('commerce_product__field_image_one', 'fimg');
		$subquery->fields('fimg',array('field_image_one_target_id'));
		$subquery->condition('fimg.entity_id', $id);
		$select = db_select('file_managed', 'fm');
		$select->addField('fm', 'uri');
		$select->condition('fm.fid', $subquery, 'in');
		$result = $select->execute();
		
		while($row = $result->fetchAssoc()) {
			$spliturl_imgone=str_replace('public://','',$row['uri']);
			array_push($img_array,$spliturl_imgone);
	 	}




	 	//Select body content with id
	 	$query_body=$connection->query("SELECT `body_value` FROM `commerce_product__body` WHERE entity_id='".$id."'");
	 	$record_body = $query_body->fetchAssoc(); 
	 	$body_content=$record_body['body_value'];
	 	



 		$qry_link_acc=$connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id 
		LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id
		LEFT JOIN commerce_product__field_stock_info  AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id
		LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_marketing_messages  AS mm ON mm.entity_id=cp.product_id LEFT JOIN commerce_product__field_new  AS fn ON fn.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id LEFT JOIN file_managed AS fm ON fm.fid =pim.field_product_image_target_id  where cp.product_id in(SELECT DISTINCT field_product_accessories_target_id from commerce_product__field_product_accessories where entity_id ='".$id."')
		ORDER BY cp.product_id");
 		
 		$item_link =array();
		while ($row = $qry_link_acc->fetchAssoc()) {

			 $product_id = $row['product_id'];
		        $status1 = 0;
		        $query1 = $connection->query("SELECT * FROM commerce_wish_list_user where product_id ='".$product_id."' and user_id = '".$user_id."' ");
		        while($row1 = $query1->fetchAssoc()){
		          $status1 = $row1['status1'];
		        } 

			$spliturl_link=str_replace('public://','',$row['uri']);
			$discount_price =round($row['field_total_price_number'],1)+((10/100)*round($row['field_total_price_number'],1));

	        $product_link = array($row['product_id'],$row['field_product_name_value'],$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],1),$spliturl_link,$status1,$row['field_partno_value'],$row['field_new_value'],round($discount_price,2),$row['field_marketing_messages_value']);
	        $item_link[] = $product_link;
	      

		}
		
 	
		$query = $connection->query("SELECT * FROM commerce_product AS cp LEFT JOIN commerce_product__field_product_name AS pn ON pn.entity_id=cp.product_id LEFT JOIN commerce_product__field_new_feature AS nf ON nf.entity_id =cp.product_id LEFT JOIN commerce_product__field_partno AS pp ON pp.entity_id =cp.product_id LEFT JOIN commerce_product__field_stock_info AS si ON si.entity_id = cp.product_id LEFT JOIN commerce_product__field_total_price AS tp ON tp.entity_id=cp.product_id LEFT JOIN commerce_product__field_category_name AS fc ON fc.entity_id=cp.product_id LEFT JOIN taxonomy_term_field_data AS tfd ON fc.field_category_name_target_id = tfd.tid LEFT JOIN commerce_product__field_color AS pc ON pc.entity_id=cp.product_id LEFT JOIN commerce_product__field_wishlist AS fw ON fw.entity_id=cp.product_id LEFT JOIN commerce_product__field_new  AS f_n ON f_n.entity_id=cp.product_id LEFT JOIN commerce_product__field_marketing_messages  AS mm ON mm.entity_id=cp.product_id LEFT JOIN commerce_product__field_product_image AS pim ON pim.entity_id =cp.product_id LEFT JOIN file_managed AS fm ON fm.fid =pim.field_product_image_target_id WHERE fc.field_category_name_target_id=(SELECT `field_category_name_target_id` FROM `commerce_product__field_category_name` WHERE `entity_id`='".$id."')");
		while ($row = $query->fetchAssoc()) {
	       $spliturl=str_replace('public://','',$row['uri']);
	       $prodname = mb_strimwidth($row['field_product_name_value'], 0, 30, "...");
	       $discount_price =round($row['field_total_price_number'],1)+((10/100)*round($row['field_total_price_number'],1));

	        $product1 = array($row['product_id'],$prodname,$row['field_new_feature_value'],$row['field_partno_value'],$row['field_stock_info_value'],round($row['field_total_price_number'],1),$spliturl,$row['field_wishlist_value'],$row['field_product_name_value'],$row['field_color_value'],$row['name'],$row['field_new_value'],round($discount_price,2),$row['field_marketing_messages_value']);
	        $items[] = $product1;
		}
		
	  	$content = array('testing contact us');

	  $review_array=[];
	  $query = $connection->query("SELECT id,name,email,rating,comment,product_id,part_no,flag,date(created_on) as created_on,created_on as order_created_on FROM catapult_review where flag=1 and product_id='".$id."' order by order_created_on desc");
	  while ($row = $query->fetchAssoc()) {
	  $review_array[] = array(
	      $row['id'],
	      $row['name'],
	      $row['email'],
	      $row['rating'],
	      $row['comment'],
	      $row['product_id'],
	      $row['part_no'],
	      $row['flag'],$row['created_on']
	    );
	  }


	  $star_array=[];
	  $star=[];
	  $tot_cnt=0;
	  $total_rating=0;
	  $query = $connection->query("SELECT rating,COUNT(rating) AS cnt FROM catapult_review WHERE product_id='".$id."' and flag=1 GROUP BY rating");
	  while ($row = $query->fetchAssoc()) {
	  	  $star[]=$row['rating'];
	  	  $tot_cnt +=$row['cnt'];
	  	  $star_array[] = array(
	      $row['rating'],
	      $row['cnt']	     
	    );
	  }
	  $new_arr = range(1,5);  
	  $mis_arr=array_diff($new_arr, $star);   
	  $count=count($star_array);    
	                                           
	 
	  foreach($mis_arr as $k=>$v){
	  	$star_array[$count+1][0]=$v;
	  	$star_array[$count+1][1]=0;
	  	$count=count($star_array);  
	  
	  }

	$total_rating=(($star_array[0][0] * $star_array[0][1]) + ($star_array[1][0] * $star_array[1][1]) + ($star_array[2][0] * $star_array[2][1]) + ($star_array[3][0] * $star_array[3][1]) + ($star_array[4][0] * $star_array[4][1]))/$tot_cnt;

	

	  usort($star_array, function($a, $b) {
		    return $a[0] <=> $b[0];
	  });
		
	  foreach($star_array as $k=>$v){

	  	
	  	$star_array[$k][1]=($tot_cnt>0 && $v[1]>0)?round((100/($tot_cnt/$v[1])),2):0;
	  }	

	  	
	  	return array(
	        '#theme' => 'product_desc',
	        '#items'=>$items,
	        '#items_desc' => $items_desc,
	        '#img_array' => $img_array,
	        '#body_content' => $body_content,
	        '#item_link' => $item_link,
	        '#interchange_array'=>$interchange_array,
	        '#user_id' => $user_id,
	        '#review' => $review_array,
	        '#star_array' => $star_array,
	        '#total_rating' => $total_rating
	        //'#title'=>'Our Article List'
	    );
  	}else{

 		die();		
  	}
  }

  public function product_make_model_year(){
  	$connection  = \Drupal::database();
  	$json_array  = [];
  	if($_POST['from'] == 'part_no'){
  		$query = $connection->query("select entity_id from commerce_product__field_partno where field_partno_value='".$_POST['id']."'");
  		while ($row = $query->fetchAssoc()) {
  			$product_id=$row['entity_id'];
  		}	
  	}else{
  		$product_id        =  $_POST['id'];
  	}
		if($_POST['id']){
	  		
	  		$product           =  \Drupal\commerce_product\Entity\Product::load($product_id);             
	      	$field_partno      =  $product->get('field_partno')->getValue()[0]['value'];  		
			$query = $connection->query("SELECT yr.year,mk.make,md.model,en.engine
			FROM catapult_application_map am INNER JOIN  catapult_engine en ON en.engine_id = am.engine_id
			INNER JOIN catapult_year yr  ON en.year_id = yr.year_id
			INNER JOIN catapult_make mk  ON en.make_id = mk.make_id
			INNER JOIN catapult_model md  ON en.model_id = md.model_id
			WHERE am.product_id='".$field_partno."'");
			while ($row = $query->fetchAssoc()) {
				/*echo "<pre>";
				print_r($row);*/
				$json_array[] = array('year'=>$row['year'],'make'=>$row['make'],'model'=>$row['model'],'engine'=>$row['engine']);
			}
			/*echo "<pre>";
			print_r($json_array);*/
		
  		} 


  

  	 
  	echo json_encode($json_array);
  	die();		
  }
  public function post_review(){
  	$connection  = \Drupal::database();
  	$user        = \Drupal::currentUser();
    $user_display_name = $user->getDisplayName();   

  	/*print_r($_POST);
  	die();*/
  	$result=$connection->query("insert into catapult_review(name,email,rating,comment,product_id,part_no,created_by,created_on) Values('".$_POST['name']."','".$_POST['email']."','".$_POST['rating']."','".$_POST['comment']."','".$_POST['product_id']."','".$_POST['part_no']."','".$user_display_name."',Now())"); 
  	$result->allowRowCount = TRUE;
	$count = $result->rowCount();
	if($count == 1){
		$mail['body']=$this->mail_msg($_POST['name'],$_POST['email']);
	    $mail['to']='mounica.a@apaengineering.com';
	    $mail['to_name']='';
	    $mail['subject']="CUSTOMER REVIEWS";
	    $this->SendMail($mail['to'],$mail['to_name'],$mail['subject'],$mail['body']);
	    echo $count;
  		exit();
	}
	




  }

  public function mail_msg($name,$email){
  	$user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $currnetusername = $user->get('name')->value;
    global $base_url;
    $html='';
    $html .='Review was posted by : '.$name." ,Email : ".$email;
    return $html;
  }

   public function SendMail($to,$to_name='',$subject='',$body,$attachment='',$cc='',$bcc=''){

    $db = \Drupal::database();
    $result=$db->query("SELECT * from tbl_smtp_setting where status='Y'");
    while ($row = $result->fetchAssoc()) {
      $smtp_settings[]=$row;
     }
     if(!isset($smtp_settings)){
        return 'Error';
     }else{

      $mail = new \PHPMailer();
      $mail->isSMTP();
      $mail->SMTPDebug = 0;
      $mail->Host = $smtp_settings[0]['hostid'];
      $mail->Port = $smtp_settings[0]['portno'];
      $mail->SMTPSecure = $smtp_settings[0]['protocol'];
      $mail->SMTPAuth = true;
      $mail->Username = $smtp_settings[0]['emailid'];
      $mail->Password = $smtp_settings[0]['password'];
      $mail->setFrom($smtp_settings[0]['emailid'], $smtp_settings[0]['username']);
      //$mail->addReplyTo('reply-box@hostinger-tutorials.com', 'Your Name');
      $mail->addAddress($to, $to_name);
      $mail->addBCC($smtp_settings[0]['emailid']);
      $mail->addBCC("samdesantos@aol.com");
      $mail->Subject = $subject;
      $mail->msgHTML($body);
      // $mail->AltBody = 'This is a plain text message body';
      /*if($attachment){
        $mail->addAttachment($attachment);
      }

      if(!empty($cc)){
        if(is_array($cc)){
          foreach ($cc as $key => $value) {
            $mail->addCC($value);
          }
        }else{
          $mail->addCC($cc);
        }
      }

      if(!empty($bcc)){
        if(is_array($bcc)){
          foreach ($bcc as $key => $value) {
            $mail->addBCC($value);
          }
        }else{
          $mail->addBCC($bcc);
        }
      }*/
      // $mail->addAttachment('test.txt');
      if (!$mail->send()) {
        //die;
        return 'Error';
      } else {
        return 'Sent';
      }
    }
    die();
  }

 
}