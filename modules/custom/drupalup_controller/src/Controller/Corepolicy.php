<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;

class Corepolicy {

  public function page() {

    $service_array = [];
    $seo_array=[];
    $nids = \Drupal::entityQuery('node')->condition('type','core_policy')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $service_array[] = array(
      $node->body->value,$ids
      );
    } 
    /*print_r($service_array);
    exit;*/

    //print_r();
    return array(
    '#theme' => 'core_policy',
    '#items'=>$service_array,
    '#seo_array' => $seo_array
    //'#title'=>'Our Article List'
    );
  }

}

?>