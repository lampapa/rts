<?php
/**
 * @file
 * Contains \Drupal\hello\HelloController.
 */

namespace Drupal\drupalup_controller\Controller;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\Query\QueryFactory;

include_once "modules/phpmailer/PHPMailer.php";
include_once "modules/phpmailer/SMTP.php";
// include_once "modules/phpmailer/Exception.php";

class OurProductDesc extends ControllerBase  {

  public function page(){
   
  if(!empty($_POST)){

      $name=!empty($_POST['name'])?$_POST['name']:"";
      $email=!empty($_POST['email'])?$_POST['email']:"";
      $company=!empty($_POST['company'])?$_POST['company']:"";
      $phone=!empty($_POST['phone'])?$_POST['phone']:"";
      $title=!empty($_POST['title'])?$_POST['title']:"";
     // $purchased_on=!empty($_POST['purchased_on'])?$_POST['purchased_on']:"";                 
     // $installation_date=!empty($_POST['installation_date'])?$_POST['installation_date']:"";
      $model_no=!empty($_POST['model_no'])?$_POST['model_no']:"";
      $serial_no=!empty($_POST['serial_no'])?$_POST['serial_no']:"";
      $part_no=!empty($_POST['part_no'])?$_POST['part_no']:"";
      $make=!empty($_POST['make'])?$_POST['make']:"";
      $model=!empty($_POST['model'])?$_POST['model']:"";
      //$mileage=!empty($_POST['mileage'])?$_POST['mileage']:"";
      $customer_name=!empty($_POST['customer_name'])?$_POST['customer_name']:"";
      $vehicle_no=!empty($_POST['vehicle_no'])?$_POST['vehicle_no']:"";
      $vehicle_info_no=!empty($_POST['vehicle_info_no'])?$_POST['vehicle_info_no']:"";
      $vocation=!empty($_POST['vocation'])?$_POST['vocation']:"";
      $db = \Drupal::database();

      $name_data='RTAllison';

      $result = $db->insert('quote_details')->fields([
        'name' => $name,
        'title' => $title,
        'company' => $company,
        'email' => $email,
        'phone' => $phone,                                   
        //'purchased_on' => $purchased_on,
        //'installation_date' => $installation_date,
        'model_no' => $model_no,
        'serial_no' => $serial_no,
        'part_no' => $part_no,
        'make' => $make,
        'model' => $model,
        //'mileage' => $mileage,
        'customer_name' => $customer_name,
        'vehicle_no' => $vehicle_no,
        'vehicle_info_no' => $vehicle_info_no,
        'vocation' => $vocation
      ])->execute();


      $subject_data='Get Quote Details of 1000 Series';
      $mail_to='info@rtsallison.com';                
     //$mail_to='vaithi.d@apaengineering.com';
      //$mail_to='saranya.m@apaengineering.com';

      $msg_details='Name : '.$name.'<br> Mail Id : '.$email.'<br>Title : '
      .$title.'<br> Company : '.$company.'<br> Phone : '.$phone.'<br> Purchased On : '.$purchased_on.'<br> Proof of Installation Date : '.$installation_date.'<br>Transmission Model Number : '.$model_no.'<br> Transmission Serial Number : '.$serial_no.'<br>Transmission Part Number : '.$part_no.'<br> Vehicle Make : '.$make.'<br>Vehicle Model : '.$model.'<br> Vehicle Mileage : '.$mileage.'<br>Customers name : '.$customer_name.'<br> Customers Vehicle Number : '.$vehicle_no.'<br>Vehicle Information Number : '.$vehicle_info_no.'<br> Vehicle vocation : '.$vocation ;

      //$cc = 'nandini.k@apaengineering.com';
      $cc = 'erich@rtsallison.com';
      $mail_status=$this->SendMail($mail_to,$name_data,$subject_data,$msg_details,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }

      $subject_data ='Request for Quote 1000 Series';

      $msg_detail= "Hi, <br> I'm Allison. Thank you for contacting RTS. <br> I have received your quote request and forwarded it to our quotation department. <br> If there are any questions, they will give you a call. <br>Otherwise, you should receive your quote within 24 hours - if not sooner, because they like to keep me happy. <br> If you have any questions, please email me at info@rtsallison.com";
      $mailId = $email;  
      $cc = '';
      $mail_status=$this->SendMail($mailId,$name_data,$subject_data,$msg_detail,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }
                          
    }
    return array(
    '#theme' => 'allison_1000_series_transmission',
    '#items'=>'',
    '#seo_array' => ''
    //'#title'=>'Our Article List'
    );
  }

  public function allison_2000_series_transmission(){
   
  if(!empty($_POST)){

      $name=!empty($_POST['name'])?$_POST['name']:"";
      $email=!empty($_POST['email'])?$_POST['email']:"";
      $company=!empty($_POST['company'])?$_POST['company']:"";
      $phone=!empty($_POST['phone'])?$_POST['phone']:"";
      $title=!empty($_POST['title'])?$_POST['title']:"";
     // $purchased_on=!empty($_POST['purchased_on'])?$_POST['purchased_on']:"";                 
     // $installation_date=!empty($_POST['installation_date'])?$_POST['installation_date']:"";
      $model_no=!empty($_POST['model_no'])?$_POST['model_no']:"";
      $serial_no=!empty($_POST['serial_no'])?$_POST['serial_no']:"";
      $part_no=!empty($_POST['part_no'])?$_POST['part_no']:"";
      $make=!empty($_POST['make'])?$_POST['make']:"";
      $model=!empty($_POST['model'])?$_POST['model']:"";
      //$mileage=!empty($_POST['mileage'])?$_POST['mileage']:"";
      $customer_name=!empty($_POST['customer_name'])?$_POST['customer_name']:"";
      $vehicle_no=!empty($_POST['vehicle_no'])?$_POST['vehicle_no']:"";
      $vehicle_info_no=!empty($_POST['vehicle_info_no'])?$_POST['vehicle_info_no']:"";
      $vocation=!empty($_POST['vocation'])?$_POST['vocation']:"";
      $db = \Drupal::database();

      $name_data='RTAllison';

      $result = $db->insert('quote_details')->fields([
        'name' => $name,
        'title' => $title,
        'company' => $company,
        'email' => $email,
        'phone' => $phone,                                   
        //'purchased_on' => $purchased_on,
        //'installation_date' => $installation_date,
        'model_no' => $model_no,
        'serial_no' => $serial_no,
        'part_no' => $part_no,
        'make' => $make,
        'model' => $model,
        //'mileage' => $mileage,
        'customer_name' => $customer_name,
        'vehicle_no' => $vehicle_no,
        'vehicle_info_no' => $vehicle_info_no,
        'vocation' => $vocation
      ])->execute();

      $subject_data ='Get Quote Details of 2000 Series';
      
      $mail_to='info@rtsallison.com';                
     //$mail_to='vaithi.d@apaengineering.com';
      //$mail_to='saranya.m@apaengineering.com';

      $msg_details='Name : '.$name.'<br> Mail Id : '.$email.'<br>Title : '
      .$title.'<br> Company : '.$company.'<br> Phone : '.$phone.'<br> Purchased On : '.$purchased_on.'<br> Proof of Installation Date : '.$installation_date.'<br>Transmission Model Number : '.$model_no.'<br> Transmission Serial Number : '.$serial_no.'<br>Transmission Part Number : '.$part_no.'<br> Vehicle Make : '.$make.'<br>Vehicle Model : '.$model.'<br> Vehicle Mileage : '.$mileage.'<br>Customers name : '.$customer_name.'<br> Customers Vehicle Number : '.$vehicle_no.'<br>Vehicle Information Number : '.$vehicle_info_no.'<br> Vehicle vocation : '.$vocation ;

      
      $cc = 'erich@rtsallison.com';
      //$cc = 'nandini.k@apaengineering.com';
      $mail_status=$this->SendMail($mail_to,$name_data,$subject_data,$msg_details,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }

      $subject_data ='Request for Quote 2000 Series';
      
      $msg_detail= "Hi, <br> I'm Allison. Thank you for contacting RTS. <br> I have received your quote request and forwarded it to our quotation department. <br> If there are any questions, they will give you a call. <br>Otherwise, you should receive your quote within 24 hours - if not sooner, because they like to keep me happy. <br> If you have any questions, please email me at info@rtsallison.com";
      $mailId = $email;  
      $cc = '';
      $mail_status=$this->SendMail($mailId,$name_data,$subject_data,$msg_detail,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }
                         
    }
    return array(
    '#theme' => 'allison_2000_series_transmission',
    '#items'=>'',
    '#seo_array' => ''
    //'#title'=>'Our Article List'
    );
  }


public function allison_3000_series_transmission(){
   
  if(!empty($_POST)){

      $name=!empty($_POST['name'])?$_POST['name']:"";
      $email=!empty($_POST['email'])?$_POST['email']:"";
      $company=!empty($_POST['company'])?$_POST['company']:"";
      $phone=!empty($_POST['phone'])?$_POST['phone']:"";
      $title=!empty($_POST['title'])?$_POST['title']:"";
     // $purchased_on=!empty($_POST['purchased_on'])?$_POST['purchased_on']:"";                 
     // $installation_date=!empty($_POST['installation_date'])?$_POST['installation_date']:"";
      $model_no=!empty($_POST['model_no'])?$_POST['model_no']:"";
      $serial_no=!empty($_POST['serial_no'])?$_POST['serial_no']:"";
      $part_no=!empty($_POST['part_no'])?$_POST['part_no']:"";
      $make=!empty($_POST['make'])?$_POST['make']:"";
      $model=!empty($_POST['model'])?$_POST['model']:"";
      //$mileage=!empty($_POST['mileage'])?$_POST['mileage']:"";
      $customer_name=!empty($_POST['customer_name'])?$_POST['customer_name']:"";
      $vehicle_no=!empty($_POST['vehicle_no'])?$_POST['vehicle_no']:"";
      $vehicle_info_no=!empty($_POST['vehicle_info_no'])?$_POST['vehicle_info_no']:"";
      $vocation=!empty($_POST['vocation'])?$_POST['vocation']:"";
      $db = \Drupal::database();

      $name_data='RTAllison';

      $result = $db->insert('quote_details')->fields([
        'name' => $name,
        'title' => $title,
        'company' => $company,
        'email' => $email,
        'phone' => $phone,                                   
        //'purchased_on' => $purchased_on,
        //'installation_date' => $installation_date,
        'model_no' => $model_no,
        'serial_no' => $serial_no,
        'part_no' => $part_no,
        'make' => $make,
        'model' => $model,
        //'mileage' => $mileage,
        'customer_name' => $customer_name,
        'vehicle_no' => $vehicle_no,
        'vehicle_info_no' => $vehicle_info_no,
        'vocation' => $vocation
      ])->execute();

     
      $subject_data ='Get Quote Details of 3000 Series';
      
      $mail_to='info@rtsallison.com';                
     //$mail_to='vaithi.d@apaengineering.com';
      //$mail_to='saranya.m@apaengineering.com';

      $msg_details='Name : '.$name.'<br> Mail Id : '.$email.'<br>Title : '
      .$title.'<br> Company : '.$company.'<br> Phone : '.$phone.'<br> Purchased On : '.$purchased_on.'<br> Proof of Installation Date : '.$installation_date.'<br>Transmission Model Number : '.$model_no.'<br> Transmission Serial Number : '.$serial_no.'<br>Transmission Part Number : '.$part_no.'<br> Vehicle Make : '.$make.'<br>Vehicle Model : '.$model.'<br> Vehicle Mileage : '.$mileage.'<br>Customers name : '.$customer_name.'<br> Customers Vehicle Number : '.$vehicle_no.'<br>Vehicle Information Number : '.$vehicle_info_no.'<br> Vehicle vocation : '.$vocation ;

      
      $cc = 'erich@rtsallison.com';
      //$cc = 'nandini.k@apaengineering.com';
      $mail_status=$this->SendMail($mail_to,$name_data,$subject_data,$msg_details,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }

      $subject_data ='Request for Quote 3000 Series';
            
      $msg_detail= "Hi, <br> I'm Allison. Thank you for contacting RTS. <br> I have received your quote request and forwarded it to our quotation department. <br> If there are any questions, they will give you a call. <br>Otherwise, you should receive your quote within 24 hours - if not sooner, because they like to keep me happy. <br> If you have any questions, please email me at info@rtsallison.com";
      $mailId = $email;  
      $cc = '';
      $mail_status=$this->SendMail($mailId,$name_data,$subject_data,$msg_detail,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }
                          
    }
    return array(
    '#theme' => 'allison_3000_series_transmission',
    '#items'=>'',
    '#seo_array' => ''
    //'#title'=>'Our Article List'
    );
  }

  public function allison_4000_series_transmission(){
   
  if(!empty($_POST)){

      $name=!empty($_POST['name'])?$_POST['name']:"";
      $email=!empty($_POST['email'])?$_POST['email']:"";
      $company=!empty($_POST['company'])?$_POST['company']:"";
      $phone=!empty($_POST['phone'])?$_POST['phone']:"";
      $title=!empty($_POST['title'])?$_POST['title']:"";
     // $purchased_on=!empty($_POST['purchased_on'])?$_POST['purchased_on']:"";                 
     // $installation_date=!empty($_POST['installation_date'])?$_POST['installation_date']:"";
      $model_no=!empty($_POST['model_no'])?$_POST['model_no']:"";
      $serial_no=!empty($_POST['serial_no'])?$_POST['serial_no']:"";
      $part_no=!empty($_POST['part_no'])?$_POST['part_no']:"";
      $make=!empty($_POST['make'])?$_POST['make']:"";
      $model=!empty($_POST['model'])?$_POST['model']:"";
      //$mileage=!empty($_POST['mileage'])?$_POST['mileage']:"";
      $customer_name=!empty($_POST['customer_name'])?$_POST['customer_name']:"";
      $vehicle_no=!empty($_POST['vehicle_no'])?$_POST['vehicle_no']:"";
      $vehicle_info_no=!empty($_POST['vehicle_info_no'])?$_POST['vehicle_info_no']:"";
      $vocation=!empty($_POST['vocation'])?$_POST['vocation']:"";
      $db = \Drupal::database();

      $name_data='RTAllison';

      $result = $db->insert('quote_details')->fields([
        'name' => $name,
        'title' => $title,
        'company' => $company,
        'email' => $email,
        'phone' => $phone,                                   
        //'purchased_on' => $purchased_on,
        //'installation_date' => $installation_date,
        'model_no' => $model_no,
        'serial_no' => $serial_no,
        'part_no' => $part_no,
        'make' => $make,
        'model' => $model,
        //'mileage' => $mileage,
        'customer_name' => $customer_name,
        'vehicle_no' => $vehicle_no,
        'vehicle_info_no' => $vehicle_info_no,
        'vocation' => $vocation
      ])->execute();

     
      $subject_data ='Get Quote Details of 4000 Series';
      
      $mail_to='info@rtsallison.com';                
     //$mail_to='vaithi.d@apaengineering.com';
      //$mail_to='saranya.m@apaengineering.com';

      $msg_details='Name : '.$name.'<br> Mail Id : '.$email.'<br>Title : '
      .$title.'<br> Company : '.$company.'<br> Phone : '.$phone.'<br> Purchased On : '.$purchased_on.'<br> Proof of Installation Date : '.$installation_date.'<br>Transmission Model Number : '.$model_no.'<br> Transmission Serial Number : '.$serial_no.'<br>Transmission Part Number : '.$part_no.'<br> Vehicle Make : '.$make.'<br>Vehicle Model : '.$model.'<br> Vehicle Mileage : '.$mileage.'<br>Customers name : '.$customer_name.'<br> Customers Vehicle Number : '.$vehicle_no.'<br>Vehicle Information Number : '.$vehicle_info_no.'<br> Vehicle vocation : '.$vocation ;

      
      $cc = 'erich@rtsallison.com';
      //$cc = 'nandini.k@apaengineering.com';
      $mail_status=$this->SendMail($mail_to,$name_data,$subject_data,$msg_details,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }

      $subject_data ='Request for Quote 4000 Series';
            
      $msg_detail= "Hi, <br> I'm Allison. Thank you for contacting RTS. <br> I have received your quote request and forwarded it to our quotation department. <br> If there are any questions, they will give you a call. <br>Otherwise, you should receive your quote within 24 hours - if not sooner, because they like to keep me happy. <br> If you have any questions, please email me at info@rtsallison.com";
      $mailId = $email;  
      $cc = '';
      $mail_status=$this->SendMail($mailId,$name_data,$subject_data,$msg_detail,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }
                          
    }
    return array(
    '#theme' => 'allison_4000_series_transmission',
    '#items'=>'',
    '#seo_array' => ''
    //'#title'=>'Our Article List'
    );
  }

  public function allison_b_series_transmission(){
   
  if(!empty($_POST)){

      $name=!empty($_POST['name'])?$_POST['name']:"";
      $email=!empty($_POST['email'])?$_POST['email']:"";
      $company=!empty($_POST['company'])?$_POST['company']:"";
      $phone=!empty($_POST['phone'])?$_POST['phone']:"";
      $title=!empty($_POST['title'])?$_POST['title']:"";
     // $purchased_on=!empty($_POST['purchased_on'])?$_POST['purchased_on']:"";                 
     // $installation_date=!empty($_POST['installation_date'])?$_POST['installation_date']:"";
      $model_no=!empty($_POST['model_no'])?$_POST['model_no']:"";
      $serial_no=!empty($_POST['serial_no'])?$_POST['serial_no']:"";
      $part_no=!empty($_POST['part_no'])?$_POST['part_no']:"";
      $make=!empty($_POST['make'])?$_POST['make']:"";
      $model=!empty($_POST['model'])?$_POST['model']:"";
      //$mileage=!empty($_POST['mileage'])?$_POST['mileage']:"";
      $customer_name=!empty($_POST['customer_name'])?$_POST['customer_name']:"";
      $vehicle_no=!empty($_POST['vehicle_no'])?$_POST['vehicle_no']:"";
      $vehicle_info_no=!empty($_POST['vehicle_info_no'])?$_POST['vehicle_info_no']:"";
      $vocation=!empty($_POST['vocation'])?$_POST['vocation']:"";
      $db = \Drupal::database();

      $name_data='RTAllison';

      $result = $db->insert('quote_details')->fields([
        'name' => $name,
        'title' => $title,
        'company' => $company,
        'email' => $email,
        'phone' => $phone,                                   
        //'purchased_on' => $purchased_on,
        //'installation_date' => $installation_date,
        'model_no' => $model_no,
        'serial_no' => $serial_no,
        'part_no' => $part_no,
        'make' => $make,
        'model' => $model,
        //'mileage' => $mileage,
        'customer_name' => $customer_name,
        'vehicle_no' => $vehicle_no,
        'vehicle_info_no' => $vehicle_info_no,
        'vocation' => $vocation
      ])->execute();

      $subject_data ='Get Quote Details of B-Series';
      
      $mail_to='info@rtsallison.com';                
     //$mail_to='vaithi.d@apaengineering.com';
     // $mail_to='saranya.m@apaengineering.com';

      $msg_details='Name : '.$name.'<br> Mail Id : '.$email.'<br>Title : '
      .$title.'<br> Company : '.$company.'<br> Phone : '.$phone.'<br> Purchased On : '.$purchased_on.'<br> Proof of Installation Date : '.$installation_date.'<br>Transmission Model Number : '.$model_no.'<br> Transmission Serial Number : '.$serial_no.'<br>Transmission Part Number : '.$part_no.'<br> Vehicle Make : '.$make.'<br>Vehicle Model : '.$model.'<br> Vehicle Mileage : '.$mileage.'<br>Customers name : '.$customer_name.'<br> Customers Vehicle Number : '.$vehicle_no.'<br>Vehicle Information Number : '.$vehicle_info_no.'<br> Vehicle vocation : '.$vocation ;

      
      $cc = 'erich@rtsallison.com';
      // $cc = 'nandini.k@apaengineering.com';
      $mail_status=$this->SendMail($mail_to,$name_data,$subject_data,$msg_details,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }

      $subject_data ='Request for Quote B-Series';
      
      $msg_detail= "Hi, <br> I'm Allison. Thank you for contacting RTS. <br> I have received your quote request and forwarded it to our quotation department. <br> If there are any questions, they will give you a call. <br>Otherwise, you should receive your quote within 24 hours - if not sooner, because they like to keep me happy. <br> If you have any questions, please email me at info@rtsallison.com";
      $mailId = $email;  
      $cc = '';
      $mail_status=$this->SendMail($mailId,$name_data,$subject_data,$msg_detail,$cc);

      if($mail_status=='Error'){
        $error='Mail Sending Failed';
      }elseif($mail_status=='Sent'){
        $success='Message sent!';
      }else{
        $error='Something Went Wrong';
      }
                         
    }
    return array(
    '#theme' => 'allison_b_series_transmission',
    '#items'=>'',
    '#seo_array' => ''
    //'#title'=>'Our Article List'
    );
  }

  public function SendMail($to,$to_name='',$subject,$body,$cc,$attachment='',$bcc=''){

    $db = \Drupal::database();
    $result=$db->query("SELECT * from tbl_smtp_setting where status='Y'");
    while ($row = $result->fetchAssoc()) {
      $smtp_settings[]=$row;
     }
     if(!isset($smtp_settings)){
        return 'Error';
     }else{
          
      $mail = new \PHPMailer();
      $mail->isSMTP();
      $mail->SMTPDebug = 0;
      $mail->Host = $smtp_settings[0]['hostid'];
      $mail->Port = $smtp_settings[0]['portno'];
      $mail->SMTPSecure = $smtp_settings[0]['protocol'];
      $mail->SMTPAuth = true;
      $mail->Username = $smtp_settings[0]['emailid'];
      $mail->Password = $smtp_settings[0]['password'];
     // $mail->setFrom('tech@apaengineerings.onmicrosoft.com', 'RTSAllision : Contact Us');
      $mail->setFrom($smtp_settings[0]['emailid'], $smtp_settings[0]['username']);
      //$mail->addReplyTo('reply-box@hostinger-tutorials.com', 'Your Name');
      $mail->addAddress($to, 'RTSAllision');
  //    $mail->addAddress('erich@rtsallison.com');
    //  $cc ='catapult@apaengineering.com';
     //$mail->addAddress('balaji.kr@apaengineering.com');

     
     // $cc ='balajikr6@gmail.com';
      $mail->Subject = $subject;
      //$mail->addCC('vaithi.d@apaengineering.com');
      //$mail->addCC('nandini.k@apaengineering.com');

      $mail->addCC($cc);
    
     // $mail->Subject = 'RTS Allision - Get Quote Details';
      $mail->msgHTML($body);
      // $mail->AltBody = 'This is a plain text message body';
      if($attachment){
        $mail->addAttachment($attachment);
      }
      /*if(is_array($cc)){
        foreach ($cc as $key => $value) {
          $mail->addCC($value);
        }
      }else{
        $mail->addCC($cc);
      } 
      */
     /* if(is_array($bcc)){
        foreach ($bcc as $key => $value) {
          $mail->addBCC($value);
        }
      }else{
        $mail->addBCC($attachment);
      }*/
      // $mail->addAttachment('test.txt');
      if (!$mail->send()) {
        
        // $error='Mailer Error:' . $mail->ErrorInfo;
        return 'Error';
      } else {
        return 'Sent';
      }
    }
  }
}