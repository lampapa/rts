<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;

class TestimonialSetting{

  	public function page(){

  		
	  	global $base_url;
	  	$success_status = "";
	  	if(isset($_SESSION['postid']) ){
	      if($_SESSION['postid'] == ""){
	        $_SESSION['postid'] = rand(10,100);
	      }
	    }else{
	      $_SESSION['postid'] = rand(10,100);
	    }   
		$success_status = "";
	  	$upload_error = "";
	  	$error = "";
	  	$editor_validate = "";
	  	if(!empty($_POST)){
	      	if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
		        if($_SESSION['postid'] == $_POST['postid']){		          	
			  		$value    		   = $_POST['editor1'];
			  		if($value == ""){
		  				$editor_validate = "Please Enter Content";
		  			}else{
						$field_location = $_POST['field_location'];
						$field_testimonial_name	= $_POST['field_testimonial_name'];
						$field_testimonial_sequence	= $_POST['field_testimonial_sequence'];			
						$altext = $_POST['altext'];							
						if($_FILES["newf"]["tmp_name"] != ""){
							$name = $_FILES["newf"]["name"];					
							$exts = explode(".", $name);
							$extension = $exts[1];
							$allowedExts = array("jpeg", "jpg", "png","PNG","JPEG",'jpg');
							if(in_array($extension, $allowedExts)){
								$target_file =  basename($_FILES["newf"]["name"]);
								move_uploaded_file($_FILES["newf"]["tmp_name"], $target_file);
								//chmod($_FILES["newf"]["name"],0777);
								$data = file_get_contents($base_url."/".$_FILES["newf"]["name"]);
								$file = file_save_data($data, "public://".$_FILES["newf"]["name"], FILE_EXISTS_REPLACE);
							}else{
								$upload_error = "File Type Should Be jpg,png";
							}
						}	
				  		if( (isset($_POST['hidden_id'])) && ($_POST['hidden_id'] != "")){
				  			$node                         = Node::load($_POST['hidden_id']);			
							$node->body->value            = $value;
							$node->body->format           = 'full_html';
							$node->title                  = $field_testimonial_name;						
							$node->field_testimonial_sequence = $field_testimonial_sequence;
							$node->field_location = $field_location;
							if($upload_error == ""){
								if($_FILES["newf"]["tmp_name"] != ""){	
									$field_customerimage = array(
															    'target_id' => $file->id(),
															    'alt' => $altext,
															    'title' => "My title"
																);
									$node->field_customerimage = $field_customerimage;
								}	
								$node->save();
								$success_status = "Testimonial Updated Successfully";
							}							
				  		}else{ 								   
						    if( ($_FILES["newf"]["name"] != "") && ($upload_error == "") ){
						    	 $body = [
										    'value' => $value,
										    'format' => 'basic_html',
						    			 ];	
								$node = Node::create([
									'type'  => 'testimonial',
									'title'	=> $field_testimonial_name,
									'body'	=> ['value'=> $value,'format'=> 'basic_html'],
									'field_location' => $field_location,
									'field_testimonial_sequence'=>$field_testimonial_sequence,
								  	'field_customerimage' => [
															    'target_id' => $file->id(),
															    'alt' => $altext,
															    'title' => 'Sample File'
								  							],
								]);
								$node->save();
								chmod($_FILES["newf"]["name"],0777);
								unlink($_FILES["newf"]["name"]);
								$success_status = "Testimonial Added Successfully";
							}else{
								$error = "please upload file";							
							}
						}
					}	
					
				}
			}
			$_SESSION['postid'] = "";					
	  	} 
	  	if($_SESSION['postid'] == ""){
	  		$_SESSION['postid'] = rand(10,100);      
		}
		if($upload_error != ""){
			$error = $upload_error;
		}
		if($editor_validate != ""){
			$error = $editor_validate;
		}
    	return array('#theme' => 'testimonial_setting',
    				 '#title' => $success_status,
	    			'#postid'=>$_SESSION['postid'],
	    			'#error'=>$error	
    				);
  	}

  	public function testimonialurl(){ 	
		$service_array =[];
	  	$nids          = \Drupal::entityQuery('node')->condition('type','testimonial')->execute();
	  	/*echo "<pre>";
	  	print_r($nids);*/
	  	foreach($nids as $key => $ids){	  		
  			$node = \Drupal\node\Entity\Node::load($ids);				
			$res = $node->field_testimonial_sequence->getValue();
			if(empty($res)){
				$res = "";
			}else{
				$res = $res[0]['value'];	
			}		
			$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>strip_tags($node->body->value),
						        'file'=>file_create_url($node->field_customerimage->entity->getFileUri()),
						        'sequence'=>$res,
						        'id'=>$ids
        						);
					
		}
		echo json_encode($service_array);	
		exit();	
  	}

  	public function deletetestimonial(){ 
	  	/*$result = \Drupal::entityQuery('node')
	      ->condition('type', 'testimonial')
	      ->execute();*/
	      $nodeid = $_POST['id'];
	      $res = array($nodeid=>$nodeid);      
	      entity_delete_multiple('node', $res);
		die();
  	}

  	public function testimonialedit(){ 
	  	$id   = $_POST['id'];
	  	$node = \Drupal\node\Entity\Node::load($id);
		$res  = $node->field_testimonial_sequence->getValue();
		$location  = $node->field_location->getValue();

		$service_array[] = array(
						        'title'=>$node->getTitle(),
						        'body'=>strip_tags($node->body->value),
						        'file'=>file_create_url($node->field_customerimage->entity->getFileUri()),
						        'sequence'=>$res[0]['value'],
						        'field_location'=>$location[0]['value'],
						        'id'=>$id,
						        'altvalue'=>$node->field_customerimage->alt
	    						);
		echo json_encode($service_array);
		exit();
	}
}