<?php
namespace Drupal\site_admin\Controller;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\taxonomy\Entity\Term;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\Core\Link;
           use Drupal\Core\Url;


class CustomPage{
  public function page(){
    global $base_url;
    $success_status = "";
    $error = "";
    $editor_validate = "";
    $success_status = "";
   if(isset($_SESSION['postid']) ){
      if($_SESSION['postid'] == ""){
        $_SESSION['postid'] = rand(10,100);
      }
    }else{
      $_SESSION['postid'] = rand(10,100);
    }   
    if(!empty($_POST)){
      if( (isset($_POST['postid'])) && ($_POST['postid'] != "")  ){
        if($_SESSION['postid'] == $_POST['postid']){
          $page_name        = $_POST['page_name'];
          $editor1          = $_POST['editor1'];   
          $url          = $_POST['menu_url'];   
          $menu_name          = $_POST['menu_name']; 
         if($_POST['hidden_id'] != ""){
          $menu_id=$_POST['menu_id'];
          $menu_link = \Drupal::entityTypeManager()->getStorage('menu_link_content')->load($menu_id);
          $men_title=$menu_link->getTitle();
          $mylink = Url::fromUri($menu_link->link[0]->uri);
          $men_url= $mylink->toString();
          if($men_title == $menu_name && $men_url == $url){
                    //echo "same menu";
                $node  = Node::load($_POST['hidden_id']);
                $node->body->value            =  $editor1;
                $node->body->format           = 'full_html';
                $node->title                  = $page_name;
                $node->field_menu_name->value = $menu_name;
                $node->field_url->value = $url;
                $node->save();
              }else{
                    //delete menu
                    $menu_link = MenuLinkContent::load($menu_id);
                    $menu = array($menu_id=>$menu_id);
                    entity_delete_multiple('menu_link_content', $menu);

                    //add new menu
                    $menu =  MenuLinkContent::create([
                          'title' => $menu_name,
                          'link' => ['uri' => $url],
                          'menu_name' => 'main',
                        ]);
                    $menu->save();
                    $menu_id = $menu->id();

                    $node  = Node::load($_POST['hidden_id']);
                    $node->body->value            =  $editor1;
                    $node->body->format           = 'full_html';
                    $node->title                  = $page_name;
                    $node->field_menu_name->value = $menu_name;
                    $node->field_menu_id->value = $menu_id;
                    $node->field_url->value = $url;
                    $node->save();



              }
                  
            $success_status = "Page Updated Successfully"; 
           }else{
             $connection = \Drupal::database();
             $menu_exist = $connection->query("SELECT id FROM menu_link_content_data where title='".$menu_name."' and link__uri='".$url."'")->fetchAssoc();

             if($menu_exist['id']){
                  $menu_id = $menu_exist['id'];
                  $node = Node::create([
                              'type'  => 'custom',
                              'title' => $page_name,
                              'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                              'field_menu_name' => $menu_name,
                              'field_url' => $url,
                              'field_menu_id' => $menu_id            
                            ]);
                  $node->save(); 
             }else{
                //echo "not";
                  $menu =  MenuLinkContent::create([
                          'title' => $menu_name,
                          'link' => ['uri' => $url],
                          'menu_name' => 'main',
                        ]);
                  $menu->save();
                  $menu_id = $menu->id();
                  $node = Node::create([
                              'type'  => 'custom',
                              'title' => $page_name,
                              'body'  => ['value'=> $editor1,'format'=> 'basic_html'],
                              'field_menu_name' => $menu_name,
                              'field_url' => $url,
                              'field_menu_id' => $menu_id            
                            ]);
                $node->save(); 
             }
            $success_status = "Page Created Successfully";    

           } 
                               
        }
      }  
      $_SESSION['postid'] = "";
    }  
    if($_SESSION['postid'] == ""){
      $_SESSION['postid'] = rand(10,100);      
    }    
    return array(
        '#theme' => 'custom_page',
        '#postid' => $_SESSION['postid'],
        '#title' => $success_status,
        '#error'=>$error
    );
  }  
 
  public function fullcustompage(){
    $service_array = [];
    $nids          = \Drupal::entityQuery('node')->condition('type','custom')->execute();

    foreach($nids as $key => $ids){
      $node            = \Drupal\node\Entity\Node::load($ids);     
      /*print_r($node);
      die();*/
      $menu_n = $node->field_menu_name->getValue();
      $menu_r = $node->field_url->getValue();
      $menu_id = $node->field_menu_id->getValue();
      $menu_n = (empty($menu_n)) ? "" : $menu_n[0]['value'];
      $menu_r = (empty($menu_r)) ? "" : $menu_r[0]['value'];
      $menu_id = (empty($menu_id)) ? "" : $menu_id[0]['value'];
      $service_array[] = array(
                      'title'=>$node->getTitle(),
                      'body'=>strip_tags($node->body->value),
                      'menu_name'=>$menu_n,
                      'url'=>$menu_r,                      
                      'id'=>$ids,
                      'menu_id'=>$menu_id
                      );
    }
    echo json_encode($service_array); 
    exit(); 
  }

  public function deletecustom(){
    
      $nodeid = $_POST['tid'];
      $menu_id = $_POST['menu_id'];
      $menu_link = MenuLinkContent::load($menu_id);
      /*$result = \Drupal::entityQuery('node')
        ->condition('type', 'custom')
        ->execute();  */
      $res = array($nodeid=>$nodeid);
      entity_delete_multiple('node', $res);
      $menu = array($menu_id=>$menu_id);
      entity_delete_multiple('menu_link_content', $menu);
      die();
      
  }

  public function singlecustom(){ 
    $id   = $_POST['id'];
    $node = \Drupal\node\Entity\Node::load($id);
    $menu_n = $node->field_menu_name->getValue();
    $menu_r = $node->field_url->getValue();
    $menu_id = $node->field_menu_id->getValue();
    $menu_n = (empty($menu_n)) ? "" : $menu_n[0]['value'];
    $menu_r = (empty($menu_r)) ? "" : $menu_r[0]['value'];
    $menu_id = (empty($menu_id)) ? "" : $menu_id[0]['value'];
    
    $cus_array[] = array(
                    'title'=>$node->getTitle(),
                    'body'=>strip_tags($node->body->value),
                    'menu_name'=>$menu_n,
                    'url'=>$menu_r,
                    'id'=>$id,
                    'menu_id'=>$menu_id
                   );
    echo json_encode($cus_array);
    exit();
  }
}