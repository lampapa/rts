<?php

/**
 * @file
 * Contains \Drupal\my_custom_block\Plugin\Block
 */

namespace Drupal\my_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "my_custom_block",
 *  admin_label = @Translation("My Custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class CustomBlock extends BlockBase{
  public function build(){
      return [
      '#theme' => 'template',
      '#test_var' => $this->t('Test Value'),
    ];
  }
   public function bannerbuild(){
      return [
      '#theme' => 'banner_template',
      '#test_var' => $this->t('Test Value'),
    ];
  }
}

?>