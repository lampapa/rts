<?php

/**
 * @file
 * Contains \Drupal\maininformation_custom_block\Plugin\Block
 */

namespace Drupal\maininformation_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "maininformation_custom_block",
 *  admin_label = @Translation("Main Information Custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class MainInformationBlock extends BlockBase{
  public function build(){

    $information_array = '';
    $nids = \Drupal::entityQuery('node')->condition('type','maininformation')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $information_array = $node->body->value;
    } 
    return array(
    '#theme' => 'maininformationtemplate',
    '#test_var' => $this->t('Test Value'),
    '#items'=>$information_array
    );
  }
   
}

?>