<?php

/**
 * @file
 * Contains \Drupal\homepage_custom_block\Plugin\Block
 */

namespace Drupal\homepage_custom_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * below section is important
 * 
 * @Block(
 *  id = "homepage_custom_block",
 *  admin_label = @Translation("Home Page Custom Block"),
 *  category = @Translation("Custom")
 * ) 
 */
 
class HomePageBlock extends BlockBase{
  public function build(){

    $home_array = '';
    $nids = \Drupal::entityQuery('node')->condition('type','home_page')->execute();
    foreach ($nids as $key => $ids) {
      $node = \Drupal\node\Entity\Node::load($ids);   
      $home_array = $node->body->value;
    } 
    return array(
    '#theme' => 'homepagetemplate',
    '#test_var' => $this->t('Test Value'),
    '#items'=>$home_array
    );
  }
   
}

?>