<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/site_admin/templates/location-setting.html.twig */
class __TwigTemplate_b7dac57987fcc7bf82414914367731c30e5c399fa9168bcaedead1fdae65255f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 7];
        $filters = ["escape" => 8];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- <script src=\"https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js\"></script> -->
<div ng-app=\"postApp\" ng-cloak ng-controller=\"postController\">
            <div class=\"content\">
               <div class=\"container-fluid\">
                  <div class=\"page-title-box\">
                    <h4 class=\"page-title\">location</h4>
                     <div  class=\"alert alert-success\" ";
        // line 7
        if ((($context["title"] ?? null) == "")) {
            echo " style=\"display:none\" ";
        }
        echo ">
                        ";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
        echo "
                      </div>
                      <div  class=\"alert alert-danger\" ";
        // line 10
        if ((($context["error"] ?? null) == "")) {
            echo " style=\"display:none;color:red\" ";
        }
        echo ">
                        ";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["error"] ?? null)), "html", null, true);
        echo "
                      </div>
                    <div class=\"float-right\">
                      
                      <a href=\"javascript:history.back()\" class=\"button-normal btn-back-icon\">Back</a>
                    </div>
                  </div>
                  <div class=\"row\">
                    <div class=\"col-md-12\">
                      <div class=\"card mt-10\">
                        <div class=\"card-header card-header-toggle\">
                          <h5 class=\"card-title\"><span>Click Here to </span><font id=\"updateservice\"> Add location
                          </font></h5>
                        </div>

                          <div class=\"card-body card-body-toggle\">
                            <div class=\"row\">
                              <div class=\"col-md-7\">
                                <form action=\"location\" id=\"resetform\" method=\"post\" enctype=\"multipart/form-data\">
                                  <div class=\"form-group row\">
                                    <div class=\"col-md-3\">

                                       <label class=\"label-text\">Heading<span class=\"red-text\">*</span></label>

                                    </div>
                                    <div class=\"col-md-9\">
                                      <input id=\"headings\" maxlength=\"50\" placeholder=\"Maximum 50 words\" ng-model=\"heading\"  autocomplete=\"off\" required=\"\" pattern=\".*\\S+.*\" name=\"head\" type=\"text\" class=\"form-control\">
                                    </div>
                                  </div>
                                   <div class=\"form-group row\">
                                    <div class=\"col-md-3\">

                                       <label class=\"label-text\">Content<span class=\"red-text\">*</span></label>

                                     </div>
                                       <div class=\"col-md-9\">
                                       <textarea rows=\"5\" name=\"editor1\" id=\"summernote\" ></textarea>
                                       </div>
                                   </div>

                                   <div class=\"form-group row\">
                                    <div class=\"col-md-3\">

                                       <label class=\"label-text\">Detail Content<span class=\"red-text\">*</span></label>

                                     </div>
                                       <div class=\"col-md-9\">
                                       <textarea rows=\"5\" name=\"editor2\" id=\"summernote1\" ></textarea>
                                       </div>
                                   </div>
                                  

                                    <div class=\"form-group row\">
                                      <input type=\"hidden\" id=\"hidden_id\" name=\"hidden_id\">
                                    </div>
                                    <div class=\"form-group row\">
                                      <input type=\"hidden\" id=\"postid\" name=\"postid\" value='";
        // line 67
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["postid"] ?? null)), "html", null, true);
        echo "'>
                                    </div>
                                   
                                  <div class=\"form-group row\">
                                    <div class=\"col-md-3\"></div>
                                    <div class=\"col-md-9\">
                                      <div>
                                        <button class=\"button-normal btn-save-icon\" type=\"submit\">Save</button>
                                        <button class=\"button-normal btn-reset-icon resets\" type=\"reset\">Reset</button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class=\"row\">
                                    <div class=\"rel_lf mand\">* All Inputs Should  Begin Without Spaces</div>
                                  </div>
                              </form>
                            </div>
                            </div>
                          </div>
                      </div>
                      

                       <div class=\"col-md-12 pad-0\">
                      <div class=\"card\">
                      <div class=\"table-responsive\">
                      <div id=\"DataTables_Table_0_wrapper\" class=\"dataTables_wrapper\"><table class=\"table-striped table-sort dataTable\" id=\"DataTables_Table_0\" role=\"grid\">
                        <thead>
                          <tr role=\"row\">
                            <th  class=\"sorting_desc\" tabindex=\"0\" aria-controls=\"DataTables_Table_0\" rowspan=\"1\" colspan=\"1\" aria-sort=\"descending\" aria-label=\"Heading: activate to sort column ascending\" style=\"width: 380px;\">Heading</th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"DataTables_Table_0\" rowspan=\"1\" colspan=\"1\" aria-label=\"Content: activate to sort column ascending\" style=\"width: 614px;\">Content</th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"DataTables_Table_0\" rowspan=\"1\" colspan=\"1\" aria-label=\"Content: activate to sort column ascending\" style=\"width: 614px;\">Detail Content</th>
                            <th class=\"sorting_disabled\" rowspan=\"1\" colspan=\"1\" aria-label=\"Edit\" style=\"width: 46px;\">Edit</th>
                            <th class=\"sorting_disabled\" rowspan=\"1\" colspan=\"1\" aria-label=\"Delete\" style=\"width: 69px;\">Delete</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <td rowspan=\"1\" colspan=\"1\">
                              <input ng-change=\"headingsearch()\" ng-model=\"heading_search\" type=\"text\" name=\"\" class=\"search-table\">
                            </td>
                            <td rowspan=\"1\" colspan=\"1\">
                              <input ng-change=\"contentsearch()\" ng-model=\"content_search\" type=\"text\" name=\"\" class=\"search-table\">
                            </td>
                            <td rowspan=\"1\" colspan=\"1\"></td>
                            <td rowspan=\"1\" colspan=\"1\"></td>
                            <td rowspan=\"1\" colspan=\"1\"></td>
                          </tr>
                        </tfoot>
                        <tbody>
                       <tr ng-repeat=\"(key, value) in pagedItems[currentPage]\">
                          <td>";
        // line 117
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar("{{value.title}}");
        echo "</td>
                                        <td>";
        // line 118
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar("{{value.body }}");
        echo "</td>
                                        <td>";
        // line 119
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar("{{value.detailval }}");
        echo "</td>
                                       <td><a  ng-click='editourlocationblk(value.id)' class=\"edit-icons\"><img src=\"../themes/APA Admin Theme/images/icons/table-edit.png\"></a></td>
                                        <td><span  id=\"";
        // line 121
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar("{{value.id}}");
        echo "\" ng-click=\"deleteourlocationblk(value.id)\" class=\"delete-icons\" ><img src=\"../themes/APA Admin Theme/images/icons/table-delete.png\"></span></td>
                        </tr>
                        </tbody>
                      </table></div>
                      </div>
                      <div class=\"container-fluid pad-0 bg-pagination\">
                                  <ul class=\"pagination pull-right pagi_master \">
                                      <li ng-class=\"{disabled: currentPage == 0}\">
                                          <a href ng-click=\"prevPage()\">«</a>
                                      </li>
                                      <li ng-repeat=\"n in pagenos | limitTo:5\"
                                          ng-class=\"{active: n == currentPage}\"
                                      ng-click=\"setPage()\">
                                          <a href ng-bind=\"n + 1\">1</a>
                                      </li>
                                      <li ng-class=\"{disabled: currentPage == pagedItems.length - 1}\">
                                          <a href ng-click=\"nextPage()\">»</a>
                                      </li>
                                    </ul>
                                  </div>
                    </div>
                  </div>


                    </div>
                   
                    </div>

                    </div>


               </div>
            </div>


<div id=\"myModal\" class=\"modal fade\">
    <div class=\"modal-dialog modal-confirm\">
      <div class=\"modal-content\">
        <div class=\"modal-content_in\">
          <div class=\"icon-box\">
            <i data-dismiss=\"modal\" class=\"fa fa-close\"></i>
          </div>
          <h4 class=\"modal-title cclr\">Are you sure?</h4>
            <button  type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
        </div>
        <div class=\"modal-body\">
          <p  id=\"confirms\" typess=\"services\">Do you really want to delete these records?</p>
        </div>
        <div class=\"modal-footer svv\">
          <button  type=\"button\" class=\"btn btn-info\" data-dismiss=\"modal\">Cancel</button>
          <button  id=\"ourlocationid\" type=\"button\" class=\"btn btn-danger\">Delete</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    // Replace the <textarea id=\"editor1\"> with a CKEditor
    // instance, using default configuration.
    /*CKEDITOR.replace( 'editor1' );*/
  </script>
";
    }

    public function getTemplateName()
    {
        return "modules/custom/site_admin/templates/location-setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 121,  200 => 119,  196 => 118,  192 => 117,  139 => 67,  80 => 11,  74 => 10,  69 => 8,  63 => 7,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/site_admin/templates/location-setting.html.twig", "/var/www/html/rtsallison_staging/modules/custom/site_admin/templates/location-setting.html.twig");
    }
}
