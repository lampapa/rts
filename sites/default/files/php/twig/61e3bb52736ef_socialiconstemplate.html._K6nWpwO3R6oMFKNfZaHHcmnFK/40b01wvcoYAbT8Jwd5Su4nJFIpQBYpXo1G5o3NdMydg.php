<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/socialicons_custom_block/templates/socialiconstemplate.html.twig */
class __TwigTemplate_77f69f8dc30906350d364a67dca7477a97ed44b3b0bb774794b75c8d3163e9f8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = ["url" => 4];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"col-sm-12 col-md-4 col-lg-4\">
    <div class=\"footer-logo\">
        <ul class=\"footer-logo-listing\">
            <li><img src=\"";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTS/images/footer-allison.png\"></li>
            <li><img src=\"";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTS/images/footer_logo.png\"></li>
        </ul>
        <ul class=\"footer-social-listing\">
            <li><a><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a></li>
            <li><a><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a></li>
            <li><a><i class=\"fa fa-youtube-play\" aria-hidden=\"true\"></i></a></li>
        </ul>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/socialicons_custom_block/templates/socialiconstemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 5,  60 => 4,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/socialicons_custom_block/templates/socialiconstemplate.html.twig", "/var/www/html/rtsallison_staging/modules/custom/socialicons_custom_block/templates/socialiconstemplate.html.twig");
    }
}
