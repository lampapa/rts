<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/RTS/templates/page.html.twig */
class __TwigTemplate_c327123c33674d34c1b34311248b73aefcf8459fe0e8d7de394bb225d6653924 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 47];
        $functions = ["url" => 8];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en-us\">
<head>
  <meta charset=\"utf-8\">
  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
  <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet'>
  <script src=\"";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTS/js/jquery.min.js\"></script>
  <script src=\"";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTS/js/angular.min.js\"></script>

  <script>window.broadlyChat={id:\"5dd55dc096fd00001d6eb69f\"};</script><script src=\"https://chat.broadly.com/javascript/chat.js\" async></script>
  <script src=\"//assets.adobedtm.com/175f7caa2b90/33075e5c8c08/launch-4d0e6c8aab4c.min.js\" async></script>

  <!-- Global site tag (gtag.js) - Google Ads: 452597192 --> 
  <script async src=\"https://www.googletagmanager.com/gtag/js?id=AW-452597192\"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-452597192'); </script>

</head>

<div class=\"page-wraper\">
  <header class=\"site-header\">
    <div class=\"main-bar\">
      <div class=\"container-fluid clearfix\">
        <div class=\"logo-header\">
          <a>
            <img src=\"";
        // line 25
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTS/images/logo_header.png\">
          </a>

        </div>
        <div class=\"menu-header\">
          <div class=\"allison-logo\">
           <img src=\"";
        // line 31
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTS/images/allison.png\"> 
          </div>
          <div class=\"navbar-toggler-menu\">
            <button class=\"navbar-toggler navicon collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavDropdown\" aria-controls=\"navbarNavDropdown\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span></span>
            <span></span>
            <span></span>
            </button>
          </div>
          <div class=\"call-button\">
            <a><i class=\"fa fa-phone\" aria-hidden=\"true\"></i></a>
          </div>
        </div>
        <div class=\"header-nav navbar-collapse\" id=\"navbarNavDropdown\">
          <ul class=\"nav navbar-nav\">
            <li>
            ";
        // line 47
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
        echo "
          </li>
          </ul>
        </div>
      </div>
    </div>
  </header>

";
        // line 55
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "

";
        // line 57
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
        echo "

<footer class=\"footer\">
  <div class=\"container\">
    <div class=\"row\">
        ";
        // line 62
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
        echo "
        ";
        // line 63
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
        echo "
        ";
        // line 64
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])), "html", null, true);
        echo "
    </div>

    <div class=\"copy-right\">
      ";
        // line 68
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_copy", [])), "html", null, true);
        echo "
    </div>
  </div>
</footer>



<script type=\"text/javascript\">
\$(document).ready(function(){
    var _tg_guidesslider = jQuery('.main-slide-area');
        _tg_guidesslider.owlCarousel({
            loop:true,
            nav:false,
            dots:true,            
            margin:20,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        })
    })
</script>



</body>
</html>";
    }

    public function getTemplateName()
    {
        return "themes/RTS/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 68,  147 => 64,  143 => 63,  139 => 62,  131 => 57,  126 => 55,  115 => 47,  96 => 31,  87 => 25,  68 => 9,  64 => 8,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/RTS/templates/page.html.twig", "/var/www/html/rtsallison_staging/themes/RTS/templates/page.html.twig");
    }
}
