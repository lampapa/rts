<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/banner_custom_block/templates/bannertemplate.html.twig */
class __TwigTemplate_454e0f8f5c7629f25aad3251632006242bedaaea42128ad4a8a3451c6da4636e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["for" => 52, "if" => 62];
        $filters = ["escape" => 55, "striptags" => 97];
        $functions = ["url" => 30];

        try {
            $this->sandbox->checkSecurity(
                ['for', 'if'],
                ['escape', 'striptags'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- <style>
#searchResult,#searchRequest,#searchsupplier,#searchpartno{
        /*border: 1px solid #aaa;*/
        border:none !important;
        overflow-y: auto;
        width: 100%;
        margin-left: 0px;
        padding-bottom: 0px;
        min-height: 10px;
        max-height: 150px;
        margin-top: 0px;
        background: #fff;
        z-index: 99;
        padding-left: 1px;

      }
      #searchResult li,#searchRequest li,#searchsupplier li,#searchpartno li{
        background: lavender;
        padding: 4px;
        margin-bottom: 1px;
      }
      #searchResult li:nth-child(even),#searchRequest li:nth-child(even),#searchsupplier li:nth-child(even),#searchpartno li:nth-child(even){
        background: cadetblue;
        color: white;
      }
      #searchResult li:hover,#searchRequest li:hover,#searchsupplier li:hover,#searchpartno li:hover{
        cursor: pointer;
      }
</style>
<section id=\"slider\" class=\"fullheight m-0\" data-background-delay=\"3500\" data-background-fade=\"1000\" style=\"background-image: url(";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "sites/default/files/allison_slider.jpg); height: 756px;\">

\t\t<div class=\"display-table\">
\t\t<div class=\"display-table-cell vertical-align-middle\">

\t\t\t<div class=\"container text-center\">

\t\t\t\t<div class=\"rounded-logo wow fadeIn animated animated\" data-wow-delay=\"0.4s\" style=\"visibility: visible; animation-delay: 0.4s; animation-name: fadeIn;\">
\t\t\t\t\t<img src=\"";
        // line 38
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "sites/default/files/aboutus_logo.png\" alt=\"logo\">
\t\t\t\t</div>

\t\t\t\t<h1 class=\"fw-600 mt-30 mb-0 wow fadeIn animated animated\" data-wow-delay=\"0.6s\" style=\"visibility: visible; animation-delay: 0.6s; animation-name: fadeIn;\">Authorized Allison Dealer</h1>

\t\t\t</div>
\t\t</div>

\t</div> //-->
\t\t\t<!-- overlay + raster [optional] -->
<!-- <span class=\"raster overlay dark-3\"> //--><!-- dark|light overlay [0 to 9 opacity] --><!--</span>
</section> //-->
<!-- <section class=\"banner_slider m-b-5\">
    <div class=\"container\">
        ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["service_array"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 53
            echo "
              <div class=\"item active\">
                <img src=\"";
            // line 55
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "file", [])), "html", null, true);
            echo "\" alt=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "altvalue", [])), "html", null, true);
            echo "\" />
              </div>

          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo " //-->
      <!-- <div id=\"carousel\" class=\"slide carousel carousel-fade\">
        <div class=\"carousel-inner\">
          ";
        // line 61
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["service_array"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 62
            echo "            ";
            if (($context["key"] == 0)) {
                // line 63
                echo "              <div class=\"item active\">
                <img src=\"";
                // line 64
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "file", [])), "html", null, true);
                echo "\" alt=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "altvalue", [])), "html", null, true);
                echo "\" />
              </div>
            ";
            } else {
                // line 67
                echo "               <div class=\"item\">
                <img src=\"";
                // line 68
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "file", [])), "html", null, true);
                echo "\" alt=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "altvalue", [])), "html", null, true);
                echo "\" />
              </div>
            ";
            }
            // line 71
            echo "          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "
        </div>

      </div> Need close tag
    </div>
  </section> -->


<div class=\"slider-section\">
  <div class=\"slider-wrapper\">
    <div class=\"main-slide-area owl-carousel owl-theme\">
      ";
        // line 83
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["service_array"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 84
            echo "      <div class=\"slide-item-wrap\">
        <div class=\"slider-overlay\"></div>
        
        <div class=\"slider-bg-img\" style=\"background-image: url('";
            // line 87
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "file", [])), "html", null, true);
            echo "')\" ></div>
        <div class=\"container\">
          <div class=\"slider-content\">
            <div class=\"slider-img\">
              <img src=\"";
            // line 91
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
            echo "themes/RTS/images/logo_header.png\">
            </div>
            
            <h4 class=\"slider-heading\">";
            // line 94
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "heading", [])), "html", null, true);
            echo ".</h4>
            <h5 class=\"slider-subheading\">";
            // line 95
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "subheading", [])), "html", null, true);
            echo ".</h5>
            
            <p>";
            // line 97
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], "slidercontent", []))), "html", null, true);
            echo "</p> 

          </div>
        </div>
      </div>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "
    </div>
  </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "modules/custom/banner_custom_block/templates/bannertemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 103,  218 => 97,  213 => 95,  209 => 94,  203 => 91,  196 => 87,  191 => 84,  187 => 83,  174 => 72,  168 => 71,  160 => 68,  157 => 67,  149 => 64,  146 => 63,  143 => 62,  139 => 61,  134 => 58,  122 => 55,  118 => 53,  114 => 52,  97 => 38,  86 => 30,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/banner_custom_block/templates/bannertemplate.html.twig", "/var/www/html/rtsallison_staging/modules/custom/banner_custom_block/templates/bannertemplate.html.twig");
    }
}
