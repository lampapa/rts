<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/drupalup_controller/templates/warranty-info.html.twig */
class __TwigTemplate_ee3d54aab22b11807018a2b5111f89eb9116cac4f7927f4f08fd9775c4dfcb84 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["raw" => 3];
        $functions = ["url" => 5];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['raw'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<script type=\"application/ld+json\" class=\"aioseop-schema\">{\"@context\":\"https://schema.org\",\"@graph\":[{\"@type\":\"Organization\",\"@id\":\"https://www.rtsallison.com/#organization\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"sameAs\":[]},{\"@type\":\"WebSite\",\"@id\":\"https://www.rtsallison.com/#website\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"publisher\":{\"@id\":\"https://www.rtsallison.com/#organization\"}},{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/warranty/#webpage\",\"url\":\"https://www.rtsallison.com/warranty/\",\"inLanguage\":\"en-US\",\"name\":\"warranty\",\"isPartOf\":{\"@id\":\"https://www.rtsallison.com/#website\"},\"breadcrumb\":{\"@id\":\"https://www.rtsallison.com/warranty/#breadcrumblist\"},\"description\":\"Reliable transmission service providing repair or replace warranty for all kind of allison transmission\",\"datePublished\":\"2014-12-15T22:49:38+00:00\",\"dateModified\":\"2021-01-13T07:28:54+00:00\"},{\"@type\":\"BreadcrumbList\",\"@id\":\"https://www.rtsallison.com/warranty/#breadcrumblist\",\"itemListElement\":[{\"@type\":\"ListItem\",\"position\":1,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"Allison automatic transmission for sale with warranty | RTS Allison\"}},{\"@type\":\"ListItem\",\"position\":2,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/warranty/\",\"url\":\"https://www.rtsallison.com/warranty/\",\"name\":\"warranty\"}}]}]}</script>

";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["items"] ?? null)));
        echo "
<div class=\"warranty-bnr\">
\t<img src=\"";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTSALLISON/image/Warrantyinfo.jpg\">
</div>
<div class=\"container warranty_txt\">
    <h4 class=\"sub_title_ltalign\">Reliable Transmission Services (RTS) warrants to the owner of each RTS remanufactured Allison transmission, installed in an approved application, that it will repair or replace, at our option, any defective or malfunctioning part(s) of the transmission in accordance with the following terms, conditions, and limitations:
    </h4>
</div>
<div class=\"container warranty-info\">
    <ul>
      <li>All vehicle(s) with remanufactured Allison transmission(s) installed, require transmission oil cooler(s) to be properly flushed in order to be free of debris or transmission oil cooler should be replaced. All vehicle(s) must have the following items inspected and replaced if any wear or damage found at the time of transmission replacement: flex plate assemblies, engine flywheel housing, motor mounts, wiring harness(es), driveline components, shifter(s), shift cable, modulator, modulator cable as applicable to the transmission series. Transmission must be installed, and maintained in accordance with the current Allison Transmission \"Mechanics Tips\" handbook for each specific transmission series.</li>

      <li>All remanufactured Allison transmission(s) must use an Allison approved TES-389 transmission fluid. Two year warranty transmissions must use an approved Allison TES-295 specifications or (TranSynd) fluid.</li>

      <li>The warranty period shall begin on the date the remanufactured Allison transmission installed in the vehicle.</li>

      <li>This warranty covers only malfunctions resulting from debris in material or workmanship. RTS may, at its discretion, replace rather than repair the transmission or its components.</li>

      <li>The owner is responsible for the performance of regular maintenance services as specified in the current Allison transmission \"operator's Manual\" applicable for each specific transmission series.</li>
      <li>Repairs qualifying under this warranty will be performed, without charge, by any Reliable Transmission Service location within a timely manner.</li>
      </ul>
  \t</div>
  \t<div class=\"warranty-cover\">
  \t\t<div class=\"container\">
  \t\t\t<h4>This Warranty Does Not Cover:</h4>
  \t\t</div>  \t\t\t
  \t</div>
  \t<div class=\"container warranty-cover-fl\">
  \t\t
  \t\t<div class=\"warranty-lst\">
  \t\t<ul>
      \t\t<li>Loss of time , inconvenience, loss of use of the vehicle, or other consequential damage
\t\t\t</li>
      \t\t<li>Defects and damage caused as the result of any of the following</li>
      \t\t<li>Flood, collision, fire, theft, freezing, vandalism, riot, explosion, or objects striking the vehicle</li>
      \t\t<li>Misuse of the vehicle</li>
      \t\t<li>Alterations or modification of the transmission or the vehicle</li>
      \t\t<li>Damage resulting from improper storage</li>
      \t\t<li>Anything other than defects in Relaible Transmission Service material or workmanship</li>
      \t\t<li>Improper vehicle towing</li>
      \t\t<li>The replacement of normal maintanence items (such as filters, screens, and transmission fluid)</li>
      \t\t<li>Malfunctions resulting from improper transmission installation, containmination from external source, lack of an RTS approved external oil filter, or lack of performance of normal maintanence services</li>
      \t\t<li>Any RTS remanufactured Allison transmission which has been repaired by other than a Reliable Transmission Service outlet and we have determined that such repair(s) has adversely affected the performance and reliability.</li>
      \t\t
      \t</ul>
      </div>
  \t</div>
      <div class=\"series\">
      \t\t<div class=\"series-cover\">
      \t\t\t<div class=\"series-para container\">
      \t\t\t\t<p>
      \t\tAll customers have one or two year warranty options on the following transmission series. No mileage limitations. (Warranty options: one year or two year warranty is determined by the remanufactured Allison transmission purchasing option.)
      \t\t</p>
      \t\t<p>
      \t\t\t(Off-Highway applications of these series have a six (6) month warranty.)
      \t\t</p>
      \t\t\t</div>
  \t\t\t\t<div class=\"container\">
  \t\t\t\t\t<h4>Series:</h4>
  \t\t\t\t</div>\t
  \t\t\t\t<div class=\"container series-img\">
  \t\t\t\t\t<img src=\"";
        // line 64
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTSALLISON/image/image-series.jpg\">
  \t\t\t\t</div>
  \t\t\t</div>
      </div>
      <div class=\"Vocational-series\">
      \t<div class=\"Vocational-cover\">
  \t\t\t\t<div class=\"container\">
  \t\t\t\t\t<h4>All Vocational Series:</h4>
  \t\t\t\t\t<div class=\"Vocational-cover-dtl\">
  \t\t\t\t\t\tHighway Series (HS),  Pupil Transport/Shuttle Series (PTS), Rugged Duty Series (RDS), Bus Series (B), Emergency Vehicles Series (EVS), Motorhome Series (MH), Truck RV Series (TRV), Specialty Series (SP)
  \t\t\t\t\t</div>
  \t\t\t\t\t<div class=\"Vocational-cover-bdr\">
  \t\t\t\t\t\tThis warranty is the only warranty applicable to RTS remanufactured Allison transmissions and is expressly in lieu of any other warranties, express or implied, including any implied warranty of merchantability or fitness for any particular purpose. RTS does not authorize any person to create for it any other obligation or liability in connection with these transmissions. Reliable Transmission Service (RTS) shall not be liable for consequential damages resulting from breach of warranty or any implied warranty. If the transmission was replaced under warranty, the replacement transmission assumes the remainder warranty coverage of the original warranty.
  \t\t\t\t\t</div>
  \t\t\t\t</div>  \t\t\t
  \t\t</div>
      </div>
<!--       \t\t<li>
      \t\t\t<span>Series:</span> AT500 Series, MT600 Series , HT700 Series, 1000 Series,2000 Series,3000 Series, 4000 Series
      \t\t</li>
      \t\t<li>
      \t\t<span></span> Highway Series (HS),  Pupil Transport/Shuttle Series (PTS), Rugged Duty Series (RDS), Bus Series (B), Emergency Vehicles Series (EVS), Motorhome Series (MH), Truck RV Series (TRV), Specialty Series (SP)
      \t\t</li>
      \t\t<li>
      \t\tThis warranty is the only warranty applicable to RTS remanufactured Allison transmissions and is expressly in lieu of any other warranties, express or implied, including any implied warranty of merchantability or fitness for any particular purpose. RTS does not authorize any person to create for it any other obligation or liability in connection with these transmissions. Reliable Transmission Service (RTS) shall not be liable for consequential damages resulting from breach of warranty or any implied warranty. If the transmission was replaced under warranty, the replacement transmission assumes the remainder warranty coverage of the original warranty.
      \t\t</li>
 -->";
    }

    public function getTemplateName()
    {
        return "modules/custom/drupalup_controller/templates/warranty-info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 64,  64 => 5,  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/drupalup_controller/templates/warranty-info.html.twig", "/var/www/html/rtsallison_staging/modules/custom/drupalup_controller/templates/warranty-info.html.twig");
    }
}
