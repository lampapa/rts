<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/drupalup_controller/templates/core-policy.html.twig */
class __TwigTemplate_f4a7ca2fa9dc1b38b4f8c87d14e7a3d77ce348b11cf3fa86db373c15489656d2 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = ["url" => 97];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<style type=\"text/css\">

  h1 {
    height: 150px;
    text-align: center;
    background: #04387b;
    color: #ffffff;
    padding-top: 50px;
  }
  .core {
    padding-right: 80px !important;
    padding-left: 80px !important;
    margin-right: auto !important;
    margin-left: auto !important;
  }
  .container-img {
    width:400px;
  }
  .list {
    padding-left: 300px;
    font-weight: bold;
  }
 #tick-mark {
    position: relative;
    display: inline-block;
    width: 30px;
    height: 30px;
}

#tick-mark::before {
    position: absolute;
    left: 0;
    top: 50%;
    height: 50%;
    width: 3px;
    background-color: #336699;
    content: \"\";
    transform: translateX(10px) rotate(-45deg);
    transform-origin: left bottom;
}

#tick-mark::after {
    position: absolute;
    left: 0;
    bottom: 0;
    height: 3px;
    width: 100%;
    background-color: #336699;
    content: \"\";
    transform: translateX(10px) rotate(-45deg);
    transform-origin: left bottom;
}

</style>

<h1>CORE POLICY</h1>
<br/>
<div class=\"core\">
    Prompt return of all transmission cores is essential to maintaining effective inventory controls and keeping cost down.<br/><b> Therefore, it is essential to return cores as soon as possible.</b>
   
</div><br/>
<div class=\"core\">
Transmission cores must be returned to RTS within sixty (60) days from date of purchase. Cores
must be completely assembled, and not previously disassembled for inspection. Cores will be
inspected for damage to the main case, rear housing, adapter housing, torque converter, torque
converter housing, retarder housing, intregal oil cooler housing/cover, oil pan or control module, and
speed sensor(s). A charge will occur if any of these parts are cracked, broken, previously welded,
missing or damaged beyond serviceability. Any core assembly (transmission) that is not returned
within sixty (60) days (except Retarder models - thirty (30) days) will be assessed a “Non-Returned
Core Assembly” charge.
</div><br/>

<div class=\"core\">
Cores must be completely drained of fluid. All shipping plugs and torque converter straps removed
from the remanufactured Allison transmission must be installed on the core prior to returning to
RTS. Freight Policy states the cores must be returned on the same shipping pallet or the container
that the remanufactured Allison transmission was delivered on.
</div><br/>
<div class=\"core\">

Cores returned must be the same model, vintage, and configuration as ordered. Cores returned that
are a different model, vintage, or configuration will have a core charge. 
</div><br/>
<div class=\"list\" >
  The following are examples of non-acceptable cores:
</div>
  <div class=\"list\" style=\"padding-left:325px;\">
      <span style=\"color:red;\">&#10003;</span>Pre-block transmission<br/>
      <span style=\"color:red;\">&#10003;</span>Transmissions damaged due to improper towin<br/>
      <span style=\"color:red;\">&#10003;</span>Transmissions submerged under water<br/>
      <span style=\"color:red;\">&#10003;</span>Transmissions subjected to fire damage<br/>
  </div>
<br/>
<div class=\"container\">
    <div class=\"container-img\" style=\"float:left;margin-left: 150px;\">
      <h4>Pre-block transmission</h4><br/>
    \t<img src=\"";
        // line 97
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTSALLISON/image/pre-block.png\">
    </div>
    <div class= \"container-img\" style=\"float:right;margin-right: 150px;\">
      <h4>Water damage</h4><br/>
      <img src=\"";
        // line 101
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTSALLISON/image/water-damage.png\" width=\"300\">
    </div>
</div><br/>
<div class=\"container\">
    <div class=\"container-img\" style=\"float:left;margin-left: 150px;\">
     
      <img src=\"";
        // line 107
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTSALLISON/image/planetary.png\">
         <br/><h4>Result of improper towing <br/>(planetary assembly destroyed)</h4>
    </div>

    <div class= \"container-img\" style=\"float:right;margin-right: 150px;\">
      <img src=\"";
        // line 112
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTSALLISON/image/fire-damage.png\">
        <br/><h4>Fire damage</h4>
    </div>
</div><br/>
<div class=\"core\">
  <span> <b>Support: 800.344.0485 </b></span>
<span style='text-align:center;padding-left:350px;'>
  <a href=\"https://www.rtsallison.com/\" target=\"_blank\">www.rtsallison.com</a>
</span>

</div>
";
    }

    public function getTemplateName()
    {
        return "modules/custom/drupalup_controller/templates/core-policy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 112,  169 => 107,  160 => 101,  153 => 97,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/drupalup_controller/templates/core-policy.html.twig", "/var/www/html/rtsallison_staging/modules/custom/drupalup_controller/templates/core-policy.html.twig");
    }
}
