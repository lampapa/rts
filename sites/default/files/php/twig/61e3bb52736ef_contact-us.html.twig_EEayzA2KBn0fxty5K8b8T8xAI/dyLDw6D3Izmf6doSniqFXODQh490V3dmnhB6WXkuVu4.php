<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/drupalup_controller/templates/contact-us.html.twig */
class __TwigTemplate_fdea772dacc8d47cbe28aa555143c89f0803c2fba32e3e288dc6dc4afbfe7cb4 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["for" => 22, "if" => 23];
        $filters = ["escape" => 24];
        $functions = ["url" => 21];

        try {
            $this->sandbox->checkSecurity(
                ['for', 'if'],
                ['escape'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<script type=\"application/ld+json\" class=\"aioseop-schema\">{\"@context\":\"https://schema.org\",\"@graph\":[{\"@type\":\"Organization\",\"@id\":\"https://www.rtsallison.com/#organization\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"sameAs\":[]},{\"@type\":\"WebSite\",\"@id\":\"https://www.rtsallison.com/#website\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"publisher\":{\"@id\":\"https://www.rtsallison.com/#organization\"}},{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/contact_us#webpage\",\"url\":\"https://www.rtsallison.com/contact_us\",\"inLanguage\":\"en-US\",\"name\":\"warranty\",\"isPartOf\":{\"@id\":\"https://www.rtsallison.com/#website\"},\"breadcrumb\":{\"@id\":\"https://www.rtsallison.com/contact_us#breadcrumblist\"},\"description\":\"For Replacing allison automatic transmissions contact Reliable transmission for all vehicles\",\"datePublished\":\"2014-12-15T22:49:38+00:00\",\"dateModified\":\"2021-01-13T07:28:54+00:00\"},{\"@type\":\"BreadcrumbList\",\"@id\":\"https://www.rtsallison.com/contact_us#breadcrumblist\",\"itemListElement\":[{\"@type\":\"ListItem\",\"position\":1,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"Contact Allison Transmission Dealer in United states | RTS Allison\"}},{\"@type\":\"ListItem\",\"position\":2,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/contact_us\",\"url\":\"https://www.rtsallison.com/contact_us\",\"name\":\"Contact_us\"}}]}]}</script>

<style type=\"text/css\">

 #cpatchaTextBox{
  margin-bottom: 5px;
 }
 canvas{
  pointer-events:none;
  background-color: #F8F8FF;
  padding: 2px 2px 2px 2px;
  border:1px solid #e5e7e9;
}
</style>

<div class=\"container\">
  
      <!-- Slider -->
      <div class=\"faq-page-breadcrumb\">
        <div class=\"breadcrumbs revealOnScroll\" data-animation=\"fadeIn\">
          <!-- <div class=\"breadcrumb-item\"><a href=\"";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "\">Home</a></div><i class=\"fa fa-chevron-right\"></i> -->
          ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["seo_array"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 23
            echo "          ";
            if (($this->getAttribute($context["value"], 0, [], "array") != "")) {
                // line 24
                echo "          <div class=\"breadcrumb-item current\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], 0, [], "array")), "html", null, true);
                echo "</div>
          ";
            } else {
                // line 26
                echo "           <div class=\"breadcrumb-item current\">Contact Us</div>
          ";
            }
            // line 28
            echo "          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        </div>
      </div>

      <!-- Middle Content -->
      <div class=\"contact-loc-section\">
        <!-- <h1 class=\"h1-title text-center\">H1 Tage goes here</h1>
        <h2 class=\"h2-title text-center\">H2 Tage goes here</h2> -->
        <div class=\"\">
          <div class=\"contact-page-location\">
            <div class=\"row\">
              <div class=\"col-md-6\">
             <div class=\"sde_lft\">
              <h1>Have Some Questions?</h1>
              <h2>Write Us !</h2>
              <img src=\"";
        // line 43
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTSALLISON/image/Group 37.png\">
             </div>
              
                 

              </div>
              <div class=\"col-md-6\">
                <div class=\"sde_rgt\">
                  <form class=\"form-horizontal\" role=\"form\" method=\"post\" action=\"contact_us\" id=\"contact_us_form\" onsubmit=\"return validateForm()\">
                  
                  <div class=\"form-group\">
                    <div class=\"col-sm-12\">
                      <input type=\"text\" class=\"form-control\" id=\"cus_name\" required placeholder=\"Name*\" name=\"cus_name\" value=\"\">
                    </div>
                  </div>

                  <div class=\"form-group\">
                    <div class=\"col-sm-12\">
                      <input type=\"email\" class=\"form-control\" id=\"email\" required placeholder=\"Email*\" name=\"email\" value=\"\">
                    </div>
                  </div>
                  <div class=\"form-group\">
                    <div class=\"col-sm-12\">
                      <input type=\"text\" class=\"form-control\" id=\"company\" required placeholder=\"Company*\" name=\"company\" value=\"\">
                    </div>
                  </div>
                  <div class=\"form-group\">
                    <div class=\"col-sm-12\">
                      <input type=\"text\" class=\"form-control\" id=\"phone\" required placeholder=\"Phone*\" name=\"phone\" value=\"\">
                    </div>
                  </div>
                  <div class=\"form-group\">
                    <div class=\"col-sm-12 fflx\">
                        <div class=\"radio\">
                          Business Type*
                        </div>
                          <select name=\"businesstype\" id=\"businesstype\" class=\"selectpicker\">
                              <option value=\"none\" selected disabled hidden> 
                                  Select an Option 
                              </option>
                              <option value=\"partner\">Partner Program</option>
                              <option value=\"sales\">Sales</option>
                          </select>

                    </div>
                  </div>
                  <div class=\"form-group\">
                    <div class=\"col-sm-12\">
                      <textarea class=\"form-control\" rows=\"4\" placeholder=\"Message\" id = \"message\" name=\"message\"></textarea>
                    </div>
                  </div>
                  <div class=\"form-group\">
                      <div id=\"captcha\" style=\" width: 130px;margin-bottom:10px;padding-left:25px;\">
                      </div>
                      <div class=\"col-sm-12\">
                          <input type=\"text\" class=\"form-control\" placeholder=\"Captcha*\" required id=\"cpatchaTextBox\" autocomplete=\"off\" />
                      </div>
                  </div>
                  <div class=\"bbtn\">
                  <button class=\"btn btn-primary send-button\" id=\"submit\" type=\"submit\" value=\"SEND\">
                      submit                  
                  </button>
                  </div>
                </form>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>


    </div>

<script type=\"text/javascript\">

window.onload = function() {
      createCaptcha();
    };
    var code;

function createCaptcha() {
      document.getElementById('captcha').innerHTML = \"\";
      var charsArray =\"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#\$%^&*\";
      var lengthOtp = 6;
      var captcha = [];
      for (var i = 0; i < lengthOtp; i++) {
        var index = Math.floor(Math.random() * charsArray.length + 1);
        if (captcha.indexOf(charsArray[index]) == -1)
          captcha.push(charsArray[index]);
        else i--;
      }
      var canv = document.createElement(\"canvas\");
      canv.id = \"captcha\";
      canv.width = 100;
      canv.height = 50;
      var ctx = canv.getContext(\"2d\");
      ctx.font = \"25px Georgia\";
      ctx.strokeText(captcha.join(\"\"), 0, 30);
      code = captcha.join(\"\");
      document.getElementById(\"captcha\").appendChild(canv); 
  
  }

  function validateForm() {
    if(\$('#cus_name').val() == ''){
      \$('#cus_name').css(\"border-color\",\"red\");
      return false;
    }else{
      \$('#cus_name').css(\"border-color\",\"green\");
    }

    if(\$('#email').val() == ''){
      \$('#email').css(\"border-color\",\"red\");
      return false;
    }else{
      \$('#email').css(\"border-color\",\"green\");
    }

    if(\$('#phone').val() == ''){
      \$('#phone').css(\"border-color\",\"red\");
      return false;
    }else{
      \$('#phone').css(\"border-color\",\"green\");
    } 

    if(\$('#company').val() == ''){
      \$('#company').css(\"border-color\",\"red\");
      return false;
    }else{
      \$('#company').css(\"border-color\",\"green\");
      
    } 
    var capchaval=document.getElementById(\"cpatchaTextBox\").value;
    if(capchaval == '' || capchaval == undefined){
      \$('#cpatchaTextBox').css(\"border-color\",\"red\");
    }else{
      if (capchaval == code) {
        return true;
        \$( \"#contact_us_form\" ).submit();
        
      }else{
        \$('#cpatchaTextBox').css(\"border-color\",\"red\");
        \$('#cpatchaTextBox').val(\"\");
        createCaptcha();
      }
    }
    return true; 
  }

  </script>";
    }

    public function getTemplateName()
    {
        return "modules/custom/drupalup_controller/templates/contact-us.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 43,  104 => 29,  98 => 28,  94 => 26,  88 => 24,  85 => 23,  81 => 22,  77 => 21,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/drupalup_controller/templates/contact-us.html.twig", "/var/www/html/rtsallison_staging/modules/custom/drupalup_controller/templates/contact-us.html.twig");
    }
}
