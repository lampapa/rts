<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/locations_footer_block/templates/locationstemplate.html.twig */
class __TwigTemplate_024eb5e1960061c78da0da9de9adc7517da22689fa05e5a06f2fd92c8f279e1e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["for" => 7, "if" => 8, "set" => 9];
        $filters = ["escape" => 45];
        $functions = ["url" => 45];

        try {
            $this->sandbox->checkSecurity(
                ['for', 'if', 'set'],
                ['escape'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
<!--
<div class=\"col-lg-3 col-md-3 col-sm-5 col-xs-12\"\">
            <h2 class=\"footer_title\">LOCATIONS</h2>
            <div class=\"col-sm-12\">
              <ul class=\"personal_info\">
                ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["loca"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["val"]) {
            // line 8
            echo "                ";
            if (($this->getAttribute($context["val"], 1, [], "array") == "226")) {
                // line 9
                echo "                  ";
                $context["res"] = "Riverview";
                // line 10
                echo "                ";
            }
            // line 11
            echo "
                ";
            // line 12
            if (($this->getAttribute($context["val"], 1, [], "array") == "227")) {
                // line 13
                echo "                  ";
                $context["res"] = "Jacksonville";
                // line 14
                echo "                ";
            }
            // line 15
            echo "
                ";
            // line 16
            if (($this->getAttribute($context["val"], 1, [], "array") == "228")) {
                // line 17
                echo "                  ";
                $context["res"] = "Slidell";
                // line 18
                echo "                ";
            }
            // line 19
            echo "
                ";
            // line 20
            if (($this->getAttribute($context["val"], 1, [], "array") == "229")) {
                // line 21
                echo "                  ";
                $context["res"] = "Conley";
                // line 22
                echo "                ";
            }
            // line 23
            echo "
                ";
            // line 24
            if (($this->getAttribute($context["val"], 1, [], "array") == "230")) {
                // line 25
                echo "                  ";
                $context["res"] = "Houston";
                // line 26
                echo "                ";
            }
            // line 27
            echo "
                ";
            // line 28
            if (($this->getAttribute($context["val"], 1, [], "array") == "231")) {
                // line 29
                echo "                  ";
                $context["res"] = "Indianapolis";
                // line 30
                echo "                ";
            }
            // line 31
            echo "
                ";
            // line 32
            if (($this->getAttribute($context["val"], 1, [], "array") == "246")) {
                // line 33
                echo "                  ";
                $context["res"] = "Dayton";
                // line 34
                echo "                ";
            }
            // line 35
            echo "
                ";
            // line 36
            if (($this->getAttribute($context["val"], 1, [], "array") == "248")) {
                // line 37
                echo "                  ";
                $context["res"] = "Lebanon";
                // line 38
                echo "                ";
            }
            // line 39
            echo "
                ";
            // line 40
            if (($this->getAttribute($context["val"], 1, [], "array") == "250")) {
                // line 41
                echo "                  ";
                $context["res"] = "Mobile";
                // line 42
                echo "                ";
            }
            // line 43
            echo "
                <li>
                  <a href=\"";
            // line 45
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
            echo "our_location/";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["res"] ?? null)), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["val"], 0, [], "array")), "html", null, true);
            echo "</a>
                </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['val'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "              </ul>
            </div>
          </div> //-->


      <div class=\"col-sm-4 col-md-2 col-lg-2\">
        <div class=\"footer-title\">
          Location
        </div>
        <ul class=\"footer-listing\">
          ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["loca"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["val"]) {
            // line 59
            echo "                ";
            if (($this->getAttribute($context["val"], 1, [], "array") == "226")) {
                // line 60
                echo "                  ";
                $context["res"] = "Riverview";
                // line 61
                echo "                ";
            }
            // line 62
            echo "
                ";
            // line 63
            if (($this->getAttribute($context["val"], 1, [], "array") == "227")) {
                // line 64
                echo "                  ";
                $context["res"] = "Jacksonville";
                // line 65
                echo "                ";
            }
            // line 66
            echo "
                ";
            // line 67
            if (($this->getAttribute($context["val"], 1, [], "array") == "228")) {
                // line 68
                echo "                  ";
                $context["res"] = "Slidell";
                // line 69
                echo "                ";
            }
            // line 70
            echo "
                ";
            // line 71
            if (($this->getAttribute($context["val"], 1, [], "array") == "229")) {
                // line 72
                echo "                  ";
                $context["res"] = "Conley";
                // line 73
                echo "                ";
            }
            // line 74
            echo "
                ";
            // line 75
            if (($this->getAttribute($context["val"], 1, [], "array") == "230")) {
                // line 76
                echo "                  ";
                $context["res"] = "Houston";
                // line 77
                echo "                ";
            }
            // line 78
            echo "
                ";
            // line 79
            if (($this->getAttribute($context["val"], 1, [], "array") == "231")) {
                // line 80
                echo "                  ";
                $context["res"] = "Indianapolis";
                // line 81
                echo "                ";
            }
            // line 82
            echo "
                ";
            // line 83
            if (($this->getAttribute($context["val"], 1, [], "array") == "246")) {
                // line 84
                echo "                  ";
                $context["res"] = "Dayton";
                // line 85
                echo "                ";
            }
            // line 86
            echo "
                ";
            // line 87
            if (($this->getAttribute($context["val"], 1, [], "array") == "248")) {
                // line 88
                echo "                  ";
                $context["res"] = "Lebanon";
                // line 89
                echo "                ";
            }
            // line 90
            echo "
                ";
            // line 91
            if (($this->getAttribute($context["val"], 1, [], "array") == "250")) {
                // line 92
                echo "                  ";
                $context["res"] = "Mobile";
                // line 93
                echo "                ";
            }
            // line 94
            echo "
                <li>
                  <a href=\"";
            // line 96
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
            echo "our_location/";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["res"] ?? null)), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["val"], 0, [], "array")), "html", null, true);
            echo "</a>
                </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['val'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 99
        echo "        </ul>
      </div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/locations_footer_block/templates/locationstemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  311 => 99,  298 => 96,  294 => 94,  291 => 93,  288 => 92,  286 => 91,  283 => 90,  280 => 89,  277 => 88,  275 => 87,  272 => 86,  269 => 85,  266 => 84,  264 => 83,  261 => 82,  258 => 81,  255 => 80,  253 => 79,  250 => 78,  247 => 77,  244 => 76,  242 => 75,  239 => 74,  236 => 73,  233 => 72,  231 => 71,  228 => 70,  225 => 69,  222 => 68,  220 => 67,  217 => 66,  214 => 65,  211 => 64,  209 => 63,  206 => 62,  203 => 61,  200 => 60,  197 => 59,  193 => 58,  181 => 48,  168 => 45,  164 => 43,  161 => 42,  158 => 41,  156 => 40,  153 => 39,  150 => 38,  147 => 37,  145 => 36,  142 => 35,  139 => 34,  136 => 33,  134 => 32,  131 => 31,  128 => 30,  125 => 29,  123 => 28,  120 => 27,  117 => 26,  114 => 25,  112 => 24,  109 => 23,  106 => 22,  103 => 21,  101 => 20,  98 => 19,  95 => 18,  92 => 17,  90 => 16,  87 => 15,  84 => 14,  81 => 13,  79 => 12,  76 => 11,  73 => 10,  70 => 9,  67 => 8,  63 => 7,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/locations_footer_block/templates/locationstemplate.html.twig", "/var/www/html/rtsallison_staging/modules/custom/locations_footer_block/templates/locationstemplate.html.twig");
    }
}
