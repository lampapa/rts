<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/drupalup_controller/templates/telma.html.twig */
class __TwigTemplate_abc51f3ed13a74088002c4a1632b222a3c43273d5981f24cb4227879ffc5d308 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["raw" => 3];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<script type=\"application/ld+json\" class=\"aioseop-schema\">{\"@context\":\"https://schema.org\",\"@graph\":[{\"@type\":\"Organization\",\"@id\":\"https://www.rtsallison.com/#organization\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"sameAs\":[]},{\"@type\":\"WebSite\",\"@id\":\"https://www.rtsallison.com/#website\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"publisher\":{\"@id\":\"https://www.rtsallison.com/#organization\"}},{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/telma/#webpage\",\"url\":\"https://www.rtsallison.com/telma/\",\"inLanguage\":\"en-US\",\"name\":\"telma\",\"isPartOf\":{\"@id\":\"https://www.rtsallison.com/#website\"},\"breadcrumb\":{\"@id\":\"https://www.rtsallison.com/telma/#breadcrumblist\"},\"description\":\"Telma is best frictionless braking systems for all vehicles for the peace of mind and comfort of the driver and passengers.\",\"datePublished\":\"2014-12-15T22:49:38+00:00\",\"dateModified\":\"2021-01-13T07:28:54+00:00\"},{\"@type\":\"BreadcrumbList\",\"@id\":\"https://www.rtsallison.com/telma/#breadcrumblist\",\"itemListElement\":[{\"@type\":\"ListItem\",\"position\":1,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"Telma Electromagnetic Braking Systems | RTS Allison\"}},{\"@type\":\"ListItem\",\"position\":2,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/telma/\",\"url\":\"https://www.rtsallison.com/telma/\",\"name\":\"telma\"}}]}]}</script>

";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["items"] ?? null)));
        echo "
";
    }

    public function getTemplateName()
    {
        return "modules/custom/drupalup_controller/templates/telma.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/drupalup_controller/templates/telma.html.twig", "/var/www/html/rtsallison_staging/modules/custom/drupalup_controller/templates/telma.html.twig");
    }
}
