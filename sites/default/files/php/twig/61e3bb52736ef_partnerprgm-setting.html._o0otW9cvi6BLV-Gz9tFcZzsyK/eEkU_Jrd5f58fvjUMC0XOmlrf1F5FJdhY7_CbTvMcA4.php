<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/site_admin/templates/partnerprgm-setting.html.twig */
class __TwigTemplate_1ec5c627032de0644cb9a39e5a44a359fb9e3c4db53f61d9b215cb7c83754cea extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 13];
        $filters = ["escape" => 14];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- <script src=\"https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js\"></script> -->
<style>
  .card-header{
    background: #2399f7;
    color: #fff;
  }
  </style>
<div ng-app=\"postApp\" ng-cloak ng-controller=\"postController\">
            <div class=\"content\">
               <div class=\"container-fluid\">
                  <div class=\"page-title-box\">
                    <h4 class=\"page-title\">Partner Program</h4>
                     <div  class=\"alert alert-success\" ";
        // line 13
        if ((($context["title"] ?? null) == "")) {
            echo " style=\"display:none\" ";
        }
        echo ">
                        ";
        // line 14
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
        echo "
                      </div>
                      <div  class=\"alert alert-danger\" ";
        // line 16
        if ((($context["error"] ?? null) == "")) {
            echo " style=\"display:none;color:red\" ";
        }
        echo ">
                        ";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["error"] ?? null)), "html", null, true);
        echo "
                      </div>
                    <div class=\"float-right\">
                      
                      <a href=\"javascript:history.back()\" class=\"button-normal btn-back-icon\">Back</a>
                    </div>
                  </div>
                  <div class=\"row\">
                    <div class=\"col-md-12\">
                      <div class=\"card mt-10\">
                        <div class=\"card-header show-card\">
                          <h5 class=\"\"><!-- <span>Click Here to </span> --><font id=\"updateservice\"> Partner Program
                          </font></h5>
                        </div>

                          <div class=\"card-body card-body-toggle\" style=\"display: block;\">
                            <div class=\"row\">
                              <div class=\"col-md-7\">
                                <form action=\"partnerprgm\" id=\"resetform\" method=\"post\" enctype=\"multipart/form-data\">
                                  <div class=\"form-group row\">
                                    <div class=\"col-md-3\">

                                       <label class=\"label-text\">Heading<span class=\"red-text\">*</span></label>

                                    </div>
                                    <div class=\"col-md-9\">
                                      <input id=\"headings\" maxlength=\"50\" placeholder=\"Maximum 50 words\" ng-model=\"heading\"  autocomplete=\"off\" required=\"\" pattern=\".*\\S+.*\" name=\"head\" type=\"text\" class=\"form-control\">
                                    </div>
                                  </div>
                                   <div class=\"form-group row\">
                                    <div class=\"col-md-3\">
                                                           <label class=\"label-text\">Content<span class=\"red-text\">*</span></label>

                                     </div>
                                       <div class=\"col-md-9\">
                                         <!-- <textarea  autocomplete=\"off\" pattern=\".*\\S+.*\" name=\"editor1\" required=\"\" type=\"text\" ng-model=\"contents\" class=\"form-control\"> -->
                                          <textarea rows=\"5\" name=\"editor1\" id=\"summernote\"></textarea>

                                         
                                       </div>
                                   </div>
                                  

                                    <div class=\"form-group row\">
                                      <input type=\"hidden\" id=\"hidden_id\" name=\"hidden_id\">
                                    </div>
                                    <div class=\"form-group row\">
                                      <input type=\"hidden\" id=\"postid\" name=\"postid\" value='";
        // line 64
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["postid"] ?? null)), "html", null, true);
        echo "'>
                                    </div>
                                   
                                  <div class=\"form-group row\">
                                    <div class=\"col-md-3\"></div>
                                    <div class=\"col-md-9\">
                                      <div>
                                        <button class=\"button-normal btn-save-icon\" type=\"submit\">Save</button>
                                        <button class=\"button-normal btn-reset-icon resets\" type=\"reset\">Reset</button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class=\"row\">
                                    <div class=\"rel_lf mand\">* All Inputs Should  Begin Without Spaces</div>
                                  </div>
                              </form>
                            </div>
                            </div>
                          </div>
                      </div>
                      




                    </div>
                    
                    </div>

                    </div>


               </div>
            </div>




  <script>
    
   // CKEDITOR.replace( 'editor1' );
  </script>
";
    }

    public function getTemplateName()
    {
        return "modules/custom/site_admin/templates/partnerprgm-setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 64,  86 => 17,  80 => 16,  75 => 14,  69 => 13,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/site_admin/templates/partnerprgm-setting.html.twig", "/var/www/html/rtsallison_staging/modules/custom/site_admin/templates/partnerprgm-setting.html.twig");
    }
}
