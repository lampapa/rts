<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/contactus_custom_block/templates/contactustemplate.html.twig */
class __TwigTemplate_cdceb53a63d7b749f16b677ef78e1ba29c930e29af34604341415949e6a72219 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 8];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!--
<div class=\"col-lg-4 col-md-4 col-sm-7 col-xs-12\">
            <h2 class=\"footer_title\">CORPORATE</h2>

            <ul class=\"personal_info\">
              <li>
                <i class=\"fa fa-envelope\"></i>
                Email : <a href=\"mailto:'";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "email", [], "array")), "html", null, true);
        echo "'\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "email", [], "array")), "html", null, true);
        echo "</a>
              </li>
              <li>
                <i class=\"fa fa-phone\"></i>
                Phone : +";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "phone", [], "array")), "html", null, true);
        echo "
              </li>
 
              <li>
                <i class=\"fa fa-map-marker\"></i>
                <span>
                  Address :# ";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "address1", [], "array")), "html", null, true);
        echo ", ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "address2", [], "array")), "html", null, true);
        echo " ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "city", [], "array")), "html", null, true);
        echo ", ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "state", [], "array")), "html", null, true);
        echo " ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "zip_code", [], "array")), "html", null, true);
        echo ",";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "country", [], "array")), "html", null, true);
        echo ".
                </span>
              </li>
            </ul>
          </div> //-->


<div class=\"col-sm-4 col-md-4 col-lg-4\">
        <div class=\"footer-title\">
          Corporate
        </div>
        <ul class=\"footer-listing\">
          <li><a href=\"mailto:'";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "email", [], "array")), "html", null, true);
        echo "'\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "email", [], "array")), "html", null, true);
        echo "</a></li>
          <li><a href=\"tel: ";
        // line 31
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "phone", [], "array")), "html", null, true);
        echo "\"> ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "phone", [], "array")), "html", null, true);
        echo "</a></li>
          <li><a>";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "address1", [], "array")), "html", null, true);
        echo ", ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "address2", [], "array")), "html", null, true);
        echo " <br> ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "city", [], "array")), "html", null, true);
        echo " <br> ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "state", [], "array")), "html", null, true);
        echo " ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "zip_code", [], "array")), "html", null, true);
        echo "<br>";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["con_us"] ?? null), 0, [], "array"), "country", [], "array")), "html", null, true);
        echo "</a></li>
        </ul>
      </div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/contactus_custom_block/templates/contactustemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 32,  113 => 31,  107 => 30,  82 => 18,  73 => 12,  64 => 8,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/contactus_custom_block/templates/contactustemplate.html.twig", "/var/www/html/rtsallison_staging/modules/custom/contactus_custom_block/templates/contactustemplate.html.twig");
    }
}
