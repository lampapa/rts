<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/drupalup_controller/templates/reman-allison.html.twig */
class __TwigTemplate_7d29bd477b15796fa17cda08b0ed1b808f21605edc65578fe6e06b8483467047 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["raw" => 3];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<script type=\"application/ld+json\" class=\"aioseop-schema\">{\"@context\":\"https://schema.org\",\"@graph\":[{\"@type\":\"Organization\",\"@id\":\"https://www.rtsallison.com/#organization\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"sameAs\":[]},{\"@type\":\"WebSite\",\"@id\":\"https://www.rtsallison.com/#website\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"publisher\":{\"@id\":\"https://www.rtsallison.com/#organization\"}},{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/reman_allison/#webpage\",\"url\":\"https://www.rtsallison.com/reman_allison/\",\"inLanguage\":\"en-US\",\"name\":\"reman_allison\",\"isPartOf\":{\"@id\":\"https://www.rtsallison.com/#website\"},\"breadcrumb\":{\"@id\":\"https://www.rtsallison.com/reman_allison/#breadcrumblist\"},\"description\":\"RTS allison selling quality, Vocational Value remanufactured transmissions trained technicians documenting all measurements.\",\"datePublished\":\"2014-12-15T22:49:38+00:00\",\"dateModified\":\"2021-01-13T07:28:54+00:00\"},{\"@type\":\"BreadcrumbList\",\"@id\":\"https://www.rtsallison.com/reman_allison/#breadcrumblist\",\"itemListElement\":[{\"@type\":\"ListItem\",\"position\":1,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"Remanufactured Allison complete engines for sale| RTS Allison\"}},{\"@type\":\"ListItem\",\"position\":2,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/reman_allison/\",\"url\":\"https://www.rtsallison.com/reman_allison/\",\"name\":\"reman_allison\"}}]}]}</script>

";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["items"] ?? null)));
        echo "
";
    }

    public function getTemplateName()
    {
        return "modules/custom/drupalup_controller/templates/reman-allison.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/drupalup_controller/templates/reman-allison.html.twig", "/var/www/html/rts/modules/custom/drupalup_controller/templates/reman-allison.html.twig");
    }
}
