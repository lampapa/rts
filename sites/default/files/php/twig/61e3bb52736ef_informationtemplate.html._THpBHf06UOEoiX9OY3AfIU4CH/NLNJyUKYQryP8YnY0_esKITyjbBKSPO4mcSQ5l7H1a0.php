<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/information_custom_block/templates/informationtemplate.html.twig */
class __TwigTemplate_59e3d267c5f9e87633bee1b863e00ab5d538fc45a25ab10d8cf354bf3652aa57 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["raw" => 1];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["items"] ?? null)));
        echo "
<!-- <section class=\"about_us section_border p-b-30\">
     <div class=\"container\">
\t\t\t\t\t<div class=\"text-center\">
\t\t\t\t\t\t<img class=\"img-responsive wow fadeInUp animated\" data-wow-delay=\"0.4s\" src=\"/rtsallison/sites/default/files/allison-primary.jpg\" alt=\"Allison Transmissions Corporate logo\" style=\"visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;\">
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"text-center\">
\t\t\t\t\t\t<h1>Welcome to the world of <span>Allison Transmissions</span>.</h1>
\t\t\t\t\t\t<h2 class=\"col-sm-10 col-sm-offset-1 nomargin-top nomargin-bottom weight-400\">At Reliable Transmission Service (RTS), we are dedicated to providing the best support and service for those using Allison automatic transmissions.</h2>
\t\t\t\t\t</div>
    </div>
</section>
          <section class=\"p-t-30\">
\t\t\t\t<div class=\"container\">

\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"col-sm-6 col-md-4 col-lg-4\">

\t\t\t\t\t\t\t<div class=\"box-icon box-icon-left\">
\t\t\t\t\t\t\t\t<a class=\"box-icon-title\" href=\"reman_allison.html\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-cogs\"></i>
\t\t\t\t\t\t\t\t\t<h2>Remanufactured Allisons</h2>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<p class=\"\">1000 series, 2000 series, 3000 series, and more</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-sm-6 col-md-4 col-lg-4\">

\t\t\t\t\t\t\t<div class=\"box-icon box-icon-left\">
\t\t\t\t\t\t\t\t<a class=\"box-icon-title\" href=\"services.html\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-list\"></i>
\t\t\t\t\t\t\t\t\t<h2>Services</h2>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<p class=\"\">Complete list of Allison services and more</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-sm-6 col-md-4 col-lg-4\">

\t\t\t\t\t\t\t<div class=\"box-icon box-icon-left\">
\t\t\t\t\t\t\t\t<a class=\"box-icon-title\" href=\"#\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-users \"></i>
\t\t\t\t\t\t\t\t\t<h2>Partner Program</h2>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<p class=\"\">So you want to sell more Allison products, ask about our partner program!</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-sm-6 col-md-4 col-lg-4\">

\t\t\t\t\t\t\t<div class=\"box-icon box-icon-left\">
\t\t\t\t\t\t\t\t<a class=\"box-icon-title\" href=\"#\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-exchange\"></i>
\t\t\t\t\t\t\t\t\t<h2>Exchange Program</h2>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<p class=\"\">Quality remanufactured Allison transmissions delivered right to your shop.</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-sm-6 col-md-4 col-lg-4\">

\t\t\t\t\t\t\t<div class=\"box-icon box-icon-left\">
\t\t\t\t\t\t\t\t<a class=\"box-icon-title\" href=\"telma.html\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-envira\"></i>
\t\t\t\t\t\t\t\t\t<h2>Telma</h2>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<p class=\"\">Telma Electro-magnetic Braking System. </p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-sm-6 col-md-4 col-lg-4\">

\t\t\t\t\t\t\t<div class=\"box-icon box-icon-left\">
\t\t\t\t\t\t\t\t<a class=\"box-icon-title\" href=\"warranty.html\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-trophy\"></i>
\t\t\t\t\t\t\t\t\t<h2>\"Gold\" Warranty</h2>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<p class=\"\">Our transmissions are as good as gold. Standard two year warranty</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>


\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t</section> -->
";
    }

    public function getTemplateName()
    {
        return "modules/custom/information_custom_block/templates/informationtemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/information_custom_block/templates/informationtemplate.html.twig", "/var/www/html/rtsallison_staging/modules/custom/information_custom_block/templates/informationtemplate.html.twig");
    }
}
