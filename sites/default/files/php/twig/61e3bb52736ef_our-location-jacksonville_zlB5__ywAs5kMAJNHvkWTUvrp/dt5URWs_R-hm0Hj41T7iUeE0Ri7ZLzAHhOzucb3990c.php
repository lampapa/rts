<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/drupalup_controller/templates/our-location-jacksonville.html.twig */
class __TwigTemplate_ab85d0427e2a7075c6ac3716e5d3a01c3f6db2dca335c3c3a02a77905a7cf40a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = ["url" => 8];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<script type=\"application/ld+json\" class=\"aioseop-schema\">{\"@context\":\"https://schema.org\",\"@graph\":[{\"@type\":\"Organization\",\"@id\":\"https://www.rtsallison.com/#organization\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"sameAs\":[]},{\"@type\":\"WebSite\",\"@id\":\"https://www.rtsallison.com/#website\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"RTS Allison\",\"publisher\":{\"@id\":\"https://www.rtsallison.com/#organization\"}},{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/our_location/jacksonville/#webpage\",\"url\":\"https://www.rtsallison.com/our_location/jacksonville/\",\"inLanguage\":\"en-US\",\"name\":\"Data Management\",\"isPartOf\":{\"@id\":\"https://www.rtsallison.com/#website\"},\"breadcrumb\":{\"@id\":\"https://www.rtsallison.com/our_location/jacksonville/#breadcrumblist\"},\"description\":\"Get contact and location information for Allison Transmission Dealer in jacksonville\":\"2020-11-02T11:50:17+00:00\"},{\"@type\":\"BreadcrumbList\",\"@id\":\"https://www.rtsallison.com/our_location/jacksonville/#breadcrumblist\",\"itemListElement\":[{\"@type\":\"ListItem\",\"position\":1,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/\",\"url\":\"https://www.rtsallison.com/\",\"name\":\"Transmission Repair Dealer in Jacksonville | RTS Allison\"}},{\"@type\":\"ListItem\",\"position\":2,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/our_location/\",\"url\":\"https://www.rtsallison.com/our_location/\",\"name\":\"our_location\"}},{\"@type\":\"ListItem\",\"position\":3,\"item\":{\"@type\":\"WebPage\",\"@id\":\"https://www.rtsallison.com/our_location/jacksonville/\",\"url\":\"https://www.rtsallison.com/our_location/jacksonville/\",\"name\":\"Data Management\"}}]}]}</script>

<section class=\"page-header page-header-xs dark\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<h1>Our Location</h1>
\t\t\t\t\t<!-- breadcrumbs -->
\t\t\t\t\t<ol class=\"breadcrumb\">
\t\t\t\t\t\t<li><a href=\"";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "our_location\">Our Location</a></li>
\t\t\t\t\t\t<li class=\"active\">Current Location</li>
\t\t\t\t\t</ol><!-- /breadcrumbs -->
\t\t\t\t</div>
\t\t</section>
<div class=\"custom_container\" style=\"margin: 12px 0;\">
\t<div class=\"container\">
\t
<div class=\"row\">
\t<div class=\"col-md-3 col-xs-12 col-sm-6 col-lg-3 col-xl-3 pd0\">
\t\t<div class=\"loc dtl\">
\t\t\t<div class=\"loc-img\">
\t\t\t\t<img src=\"";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTSALLISON/image/Jacksonville Location.jpg\">
\t\t\t</div>
\t\t\t<div class=\"head-cnt dtl\">
\t\t\t\t<h1>JACKSONVILLE, FLORIDA</h1>
\t\t\t\t<span>Authorized Allison Dealer</span>
\t\t\t</div>
\t\t\t<!-- <div class=\"loc-cnt\">
\t\t\t\t<p>Our Riverview location is our corporate headquarters. They are responsible for all of central Florida.</p>
\t\t\t</div> -->
\t\t\t<div class=\"loc-icons dtl\">
\t\t\t\t<div class=\"loc-phne\">
\t\t\t\t\t<div class=\"loc-phne-ins\">
\t\t\t\t\t\t<i class=\"fa fa-map-marker\"></i>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"loc-phne-cnt\">
\t\t\t\t\t\t<div class=\"ins-dtl\">
\t\t\t\t\t\t\t6740 Highway Avenue Jacksonville, 
\t\t\t\t\t\t\tFL 32254
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"loc-pos\">
\t\t\t\t\t<div class=\"loc-pos-ins\">
\t\t\t\t\t\t<i class=\"fa fa-user\"></i>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"loc-pos-cnt\">
\t\t\t\t\t\t<div class=\"ins-pos\">
\t\t\t\t\t\t\t Branch Manager : <span>Susan Melton</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"toll dtl\">
\t\t\t\t<div class=\"toll-ins\">
\t\t\t\t\t<span>Toll Free :</span>877-592-5420
\t\t\t\t</div>
\t\t\t\t<div class=\"toll-border\">
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t<div class=\"toll-ins\">
\t\t\t\t\t<span>Local :</span>904-378-1703
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"drct_tble\">
\t\t\t\t<div class=\"drct\">
\t\t\t\t\t<img src=\"";
        // line 65
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTSALLISON/image/icon.png\"><span>Get Direction</span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>
\t<div class=\"col-md-9 col-xs-12 col-sm-12 col-lg-9 col-xl-9 pd0\">
\t
\t<div class=\"mapouter\"><div class=\"gmap_canvas\"><iframe width=\"100%\" height=\"597\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?q=6740%20Highway%20Avenue%20Jacksonville%2CFL%2032254&t=&z=13&ie=UTF8&iwloc=&output=embed\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe><a href=\"https://embedgooglemap.net/mapv2/\"></a></div><style>.mapouter{position:relative;text-align:right;height:auto;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:597px;width:100%;}</style></div>
\t</div>
\t
</div>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "modules/custom/drupalup_controller/templates/our-location-jacksonville.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 65,  79 => 20,  64 => 8,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/drupalup_controller/templates/our-location-jacksonville.html.twig", "/var/www/html/rtsallison_staging/modules/custom/drupalup_controller/templates/our-location-jacksonville.html.twig");
    }
}
