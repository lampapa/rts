<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/site_admin/templates/warranty-setting.html.twig */
class __TwigTemplate_027b6383b8756a9b4ad3cf5aad5e1cdeeb8e004ddbd574f31ce026467e969451 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 13];
        $filters = ["escape" => 14];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- <script src=\"https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js\"></script> -->
<style>
  .card-header{
    background: #2399f7;
    color: #fff;
  }
  </style>
<div ng-app=\"postApp\" ng-cloak ng-controller=\"postController\">
            <div class=\"content\">
               <div class=\"container-fluid\">
                  <div class=\"page-title-box\">
                    <h4 class=\"page-title\">Warranty</h4>
                     <div  class=\"alert alert-success\" ";
        // line 13
        if ((($context["title"] ?? null) == "")) {
            echo " style=\"display:none\" ";
        }
        echo ">
                        ";
        // line 14
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
        echo "
                      </div>
                      <div  class=\"alert alert-danger\" ";
        // line 16
        if ((($context["error"] ?? null) == "")) {
            echo " style=\"display:none;color:red\" ";
        }
        echo ">
                        ";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["error"] ?? null)), "html", null, true);
        echo "
                      </div>
                    <div class=\"float-right\">
                      
                      <a href=\"javascript:history.back()\" class=\"button-normal btn-back-icon\">Back</a>
                    </div>
                  </div>
                  <div class=\"row\">
                    <div class=\"col-md-12\">
                      <div class=\"card mt-10\">
                        <div class=\"card-header  show-card\">
                          <h5 class=\"\"><!-- <span>Click Here to </span> --><font id=\"updateservice\">  Warranty
                          </font></h5>
                        </div>

                          <div class=\"card-body card-body-toggle\" style=\"display: block;\">
                            <div class=\"row\">
                              <div class=\"col-md-7\">
                                <form action=\"warranty\" id=\"resetform\" method=\"post\" enctype=\"multipart/form-data\">
                                  <div class=\"form-group row\">
                                    <div class=\"col-md-3\">

                                       <label class=\"label-text\">Heading<span class=\"red-text\">*</span></label>

                                    </div>
                                    <div class=\"col-md-9\">
                                      <input id=\"headings\" maxlength=\"50\" placeholder=\"Maximum 50 words\" ng-model=\"heading\"  autocomplete=\"off\" required=\"\" pattern=\".*\\S+.*\" name=\"head\" type=\"text\" class=\"form-control\">
                                    </div>
                                  </div>
                                   <div class=\"form-group row\">
                                    <div class=\"col-md-3\">

                                       <label class=\"label-text\">Content<span class=\"red-text\">*</span></label>

                                     </div>
                                       <div class=\"col-md-9\">
                                       <!-- <textarea  autocomplete=\"off\" pattern=\".*\\S+.*\" name=\"editor1\" required=\"\" type=\"text\" ng-model=\"contents\" class=\"form-control\"> -->
                                         <textarea rows=\"5\" name=\"editor1\" id=\"summernote\"></textarea>

                                       </textarea>
                                       </div>
                                   </div>
                                  

                                    <div class=\"form-group row\">
                                      <input type=\"hidden\" id=\"hidden_id\" name=\"hidden_id\">
                                    </div>
                                    <div class=\"form-group row\">
                                      <input type=\"hidden\" id=\"postid\" name=\"postid\" value='";
        // line 65
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["postid"] ?? null)), "html", null, true);
        echo "'>
                                    </div>
                                   
                                  <div class=\"form-group row\">
                                    <div class=\"col-md-3\"></div>
                                    <div class=\"col-md-9\">
                                      <div>
                                        <button class=\"button-normal btn-save-icon\" type=\"submit\">Save</button>
                                        <button class=\"button-normal btn-reset-icon resets\" type=\"reset\">Reset</button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class=\"row\">
                                    <div class=\"rel_lf mand\">* All Inputs Should  Begin Without Spaces</div>
                                  </div>
                              </form>
                            </div>
                            </div>
                          </div>
                      </div>
                      




                    </div>
                    <!-- <div class=\"col-md-5\">
                      <div class=\"card\">
                        <div class=\"card-header\">
                             <h3 class=\"card-title\">Selected Theme</h3>
                        </div>
                          <div class=\"card-body px-3 py-3\">
                              <div class=\"demo-item\">
                                <div class=\"demo-thumb\" style=\"background-image:url(assets/images/template_1.png)\">
                                  <a href=\"\" class=\"image\" target=\"_blank\"></a>
                                </div>
                                <div class=\"demo-content\">
                                  <span class=\"change-theme\"><a href=\"javascript://\">Change Theme</a></span>
                                  <span class=\"change-theme\"><a href=\"https://www.apagreen.com/apadesign/catapult/theme1/\" target=\"_blank\">Demo</a></span>
                                </div>
                              </div>
                           </div>
                        </div>
                      </div> -->
                    </div>

                    </div>


               </div>
            </div>


<!-- <div id=\"myModal\" class=\"modal fade\">
    <div class=\"modal-dialog modal-confirm\">
      <div class=\"modal-content\">
        <div class=\"modal-content_in\">
          <div class=\"icon-box\">
            <i data-dismiss=\"modal\" class=\"fa fa-close\"></i>
          </div>
          <h4 class=\"modal-title cclr\">Are you sure?</h4>
            <button  type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
        </div>
        <div class=\"modal-body\">
          <p  id=\"confirms\" typess=\"services\">Do you really want to delete these records?</p>
        </div>
        <div class=\"modal-footer svv\">
          <button  type=\"button\" class=\"btn btn-info\" data-dismiss=\"modal\">Cancel</button>
          <button  id=\"delid\" type=\"button\" class=\"btn btn-danger\">Delete</button>
        </div>
      </div>
    </div>
  </div> -->

  <script>
    // Replace the <textarea id=\"editor1\"> with a CKEditor
    // instance, using default configuration.
   // CKEDITOR.replace( 'editor1' );
  </script>
";
    }

    public function getTemplateName()
    {
        return "modules/custom/site_admin/templates/warranty-setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 65,  86 => 17,  80 => 16,  75 => 14,  69 => 13,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/site_admin/templates/warranty-setting.html.twig", "/var/www/html/rtsallison_staging/modules/custom/site_admin/templates/warranty-setting.html.twig");
    }
}
