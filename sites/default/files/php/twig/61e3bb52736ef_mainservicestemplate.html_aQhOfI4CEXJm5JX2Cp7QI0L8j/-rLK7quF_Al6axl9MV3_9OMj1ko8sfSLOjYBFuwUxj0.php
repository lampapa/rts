<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/mainservices_custom_block/templates/mainservicestemplate.html.twig */
class __TwigTemplate_4db1decdf530db715784309ab0856bb64a881674ef5006dd5fc1827eb888b4c7 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = ["url" => 33];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "  <section class=\"why-section\">
    <div class=\"container\">

      <div class=\"our-support\">
        <div class=\"row\">
          <div class=\"col-sm-4 col-md-4 col-lg-4\">
            <div class=\"our-support-box\">
              Service
            </div>
          </div>
          <div class=\"col-sm-4 col-md-4 col-lg-4\">
            <div class=\"our-support-box\">
              Remanufacturing
            </div>
          </div>
          <div class=\"col-sm-4 col-md-4 col-lg-4\">
            <div class=\"our-support-box\">
              Locations
            </div>
          </div>
        </div>
      </div>

      <div class=\"why-section-search clearfix\">
        <input type=\"text\" class=\"search-input-red\" placeholder=\"Help me find...\">
            <button type=\"submit\" class=\"search-button-red\">
              Go
           </button>
      </div>
      <div class=\"row\">
        <div class=\"col-lg-6\">
          <div class=\"why-section-img\">
            <img src=\"";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "themes/RTS/images/allison-product.png\">
          </div>
        </div>
        <div class=\"col-lg-6\">
          <div class=\"why-section-content\">
            <div class=\"why-section-title\">
              <h1 class=\"rts-title-white rts-title-right\">Why RTS?</h1>
              <h2 class=\"rts-subtitle-white rts-title-right\">Learn what makes us differents.</h2>
            </div>
            <p class=\"bold-para-text-white rts-title-right\">Allison Only <span class=\"normal-para-text-white\">Experience</span></p>
            <p class=\"bold-para-text-white rts-title-right\">Allison Authorized<span class=\"normal-para-text-white\">for Remanufacturing and Repair</span></p>
            <p class=\"bold-para-text-white rts-title-right\">Allison Certified <span class=\"normal-para-text-white\">RTS Technicians</span></p>
            <p class=\"bold-para-text-white rts-title-right\">Warranty and <span class=\"normal-para-text-white\">Avaliability</span></p>
          </div>
        </div>
      </div>
    </div>
      
    </div>
    
  </section>";
    }

    public function getTemplateName()
    {
        return "modules/custom/mainservices_custom_block/templates/mainservicestemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 33,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/mainservices_custom_block/templates/mainservicestemplate.html.twig", "/var/www/html/rtsallison_staging/modules/custom/mainservices_custom_block/templates/mainservicestemplate.html.twig");
    }
}
