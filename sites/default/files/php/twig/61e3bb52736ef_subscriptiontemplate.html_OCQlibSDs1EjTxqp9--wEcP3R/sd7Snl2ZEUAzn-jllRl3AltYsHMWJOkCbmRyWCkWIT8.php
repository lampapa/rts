<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/subscription_custom_block/templates/subscriptiontemplate.html.twig */
class __TwigTemplate_e5417421636ca16bab0262b3521b786d60c8ed9c5132c2ed15691d9a77a48c7f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 12];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- <div class=\"news_letter\">
        <div class=\"row\">
          <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
            <div class=\"footer_newsletter\">
              <div class=\"col-sm-8\">
                <h3 class=\"m-t-0\">Subscribe to our <span>Newsletter</span></h3>
                <p>Join RTS Allison Mailing List for Updates and Special Offerings</p>
              </div>
              <div class=\"newsletter_input col-sm-4\">
                <form action=\"\" method=\"POST\">
                  <input type=\"text\" class=\"form-contact\" name=\"email\" placeholder=\"Enter your email here...\" required>
                   <input type=\"hidden\" id=\"postid\" name=\"postid\" value='";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["postid"] ?? null)), "html", null, true);
        echo "'>
                  <button type=\"submit\" name=\"submitNewsletter\" class=\"\">
                    SUBSCRIBE
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div> //-->


<div class=\"newsletter-section\">
  <div class=\"container\">
    <div class=\"col-lg-6\">
      <div class=\"newsletter-text\">
        Sign up for our <span>newsletter</span>
      </div>
    </div>
    <div class=\"col-lg-6\">
      <div class=\"newsletter-search\">
          <form action=\"\" method=\"POST\">
            <input type=\"email\" class=\"newsletter-search-input\" name=\"email\" placeholder=\"Sign me up.\" required>
            <input type=\"hidden\" id=\"postid\" name=\"postid\" value='";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["postid"] ?? null)), "html", null, true);
        echo "'>
            <button type=\"submit\" name=\"submitNewsletter\" class=\"newsletter-search-button\">
              Go
           </button>
          </form> 
      </div>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/subscription_custom_block/templates/subscriptiontemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 35,  68 => 12,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/subscription_custom_block/templates/subscriptiontemplate.html.twig", "/var/www/html/rtsallison_staging/modules/custom/subscription_custom_block/templates/subscriptiontemplate.html.twig");
    }
}
